# PROG2053 Project - Team 30
## Group members: 
- Chi Hou Fung
- Bjørn-Tore Semb
- ~~Espen Engen~~ (withdrawn from project)
- Gaute Saastad Nogva

## Project requirements: 
- Install [Docker](https://docs.docker.com/engine/install/) (all common platforms) or [Docker Desktop](https://docs.docker.com/desktop/) (only available for Windows and Mac)
  - On linux: install [docker-compose](https://docs.docker.com/compose/install/) as well
- Install [nodeJs LTS](https://nodejs.org/en/download/). Project tested on LTS 12.18.4 and LTS 14.15.0
- It is **not** neccessary to run `$ npm i` before running `$ docker-compose up -d`

## Setup: 
- While in the root folder of the project: `$ docker-compose up -d` 
  this will take a long time the first time you do it.

The project is served on [http://localhost:8080](http://localhost:8080), it is **not** available on synonyms of localhost like 127.0.0.1 or 0.0.0.0.

## Usage:
- To shut the project down, run `$ docker-compose stop`
- To start the project after it has been shut down run `$ docker-compose start`
- To check if the project is running as it should, run `$ docker-compose ps` there should be a client, a database, a phpMyAdmin instance and a server running.


## Useful commands
Want to reset your containers and volumes fully? 
- `$ docker system prune -a --volumes`

Want to get in to a container for some reason? 
- `$ docker-compose exec <containername> sh`

## Troubleshooting
### Are you getting an error after doing `$ docker-compose up -d` saying "[...] Filesharing has been cancelled"? 
Go to Docker for Windows app (or similar) -> Settings -> Resources -> File sharing -> Add all your drives (or play around with figuring out what exactly you need).
### Are you getting an error saying "npm ERR! ch() never called"? 
Delete "package-lock.json" from the client directory, then build the client again using "docker-compose build client"

## When using the webiste
### Default users:
| Usernames 	| Passwords 	| Usertype 	|
|------------	|-----------	|----------	|
| Ferrero    	| 12345678  	| Admin    	|
| Rocher     	| 12345678  	| Mod      	|
| james      	| 12345678  	| User     	|
| greg      	| 12345678  	| User     	|

### Adminpage
All mod s and administrators have access to an admin/modpage (admins have some additional functionality but they are mostly the same).
To visit that site, go to profile page and click the button to go to the adminpage.
### Deletion of comments and posts
All posts and comments can be deleted by their creator.  
Moderators adn admins have the ability to delete comments made by normal users.  
Admins are the only one able to delete comments or posts made by mods.  
Posts made by admins can only be deleted by the admin that created them.  

### Changing a users inforamtion
Ever user can change thei rown username, email, password and profile picture on their own profile page. 
Just click profile from the navbar and you can start changing anything you want

### Editing a comment or post 
All comments and posts can be edited by the users that created them. 