
module.exports = {
    port: 8080,
    appIndex: 'index.html',
    nodeResolve: true,
    watch: true,
    open: true,
    host: '0.0.0.0',
    cors: true,
}