/**
 * Sends formdata to the server
 * 
 * @param {*} url   The path of the route to fetch
 * @param {*} data  FormData object to be sent to server
 * @param {*} raw   Whether or not the response itself or the content should be returned.
 * @returns Object of the json response or the response itself
 */
export async function serverFormData(url = '', data = {}, raw = false) {
    url = `${window.MyAppGlobals.serverURL+url}`;
    
    const response = await fetch(url, {
        method: "POST",
        credentials: "include",
        body: data
    });
    
    // If the user is not logged in, send to welcome page.
    if ( response.status === 401 ) {
        window.location.assign("/welcome.html");
    }
    
    // If raw data is requested, send the response itself
    if ( raw ) {
        return response;
    } else {
        // Else return the json object
        return response.json();
    }
}