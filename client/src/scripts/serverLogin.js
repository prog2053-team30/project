import { serverFormData } from './serverFormData.js';

/**
 * @brief Logs in a user and redirects on successful login.
 * @param {*} data 
 */
export function serverLogin(data) {
  serverFormData('login', data, true).then( Response => {
    if (Response.status === 200) {
      // Redirect to home page 
      window.location.assign("/");
    } else if (Response.status === 401) {
      //TODO: Display message explaining incorrect credentials
      console.warn("Incorrect username or password");
    } else {
      console.error("Unexpected response: " + Response.status);
      //TODO: Display error message on screen.
    }
  }).catch( Response => {
    console.error(Response)
  });
}