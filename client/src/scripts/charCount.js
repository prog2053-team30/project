
/**
 * @brief Counts and displays the number of characters of max the user has written.
 * @param {*} inElem  - Element to count characters from
 * @param {*} outElem - Element to display character count
 */
export function charCount(inElem, outElem) {
    const minChar = inElem.minLength;
    const maxChar = inElem.maxLength;
    let curLen = inElem.value.length;
    let textSuffix = ` of ${maxChar} characters.`;
    
    if (inElem.validity.tooShort || inElem.value.length === 0) {
        outElem.textContent = `Please write at least ${minChar} characters`;
        outElem.removeAttribute('style');
    } else {
        outElem.textContent = curLen + textSuffix;
        
        if (curLen === maxChar) {
            outElem.style.color = "red";
            outElem.style.fontWeight = "bold";
        } else {
            outElem.removeAttribute('style');
        }
    }
}