/**
 * Fetches an object of data from the server.
 * 
 * @param {*} url   The path of the route to fetch
 * @param {*} data  Object of data to be sent to the server
 * @param {*} raw   Whether or not the response itself or the content should be returned.
 * @returns Object of the json response or the response itself
 */
export async function serverFetch(url = '', data = {}, raw = false) {
    url = `${window.MyAppGlobals.serverURL+url}`;
    
    const response = await fetch(url, {
        method: "POST",
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    
    //TODO: Add error handling
    if (response.status === 401) {
        window.location.assign("/welcome.html");
    }
    
    // If raw data is requested, send the response itself
    if ( raw ) {
        return response;
    } else {
        // Else return the json object
        return response.json();
    }
}