/**
 * @brief Checks if a username is taken
 * @param {*} usernameRegister - Username to check existance of.
 * @returns True if the username is not taken.
 */
export async function uniqueUsername(usernameRegister) {
  let encodedUsername = "";
  let exitstUser;
  
  // Encode username for existance check
  encodedUsername = encodeURI(usernameRegister);
  
  // Checks the database if the username is taken
  exitstUser = await fetch(`${window.MyAppGlobals.serverURL}checkUserExists?name=${encodedUsername}`)
    .then( Response => {
      return Response.text();
    })
    .catch( Response => {
      console.error(Response);
  });
  
  return exitstUser === "false";
}