/**
 * Fetches a json file from window.MyAppGlobals.staticInfoPath.
 * 
 * @return Promise.resolve with the json data, or Promise.reject with error msg.
 */
export async function fetchStatic() {
    const fetchResponse = await fetch(MyAppGlobals.staticInfoPath);
    
    // Check if the file was found
    if (fetchResponse.ok) {
        // Get the json data from the file
        const jsonData = await fetchResponse.json();
        // Return the promise as resolved with the json data.
        return Promise.resolve(jsonData);
    } else {
        // Error with the response, send promise reject with error msg.
        return Promise.reject("Problem fetching JSON file");
    }
}