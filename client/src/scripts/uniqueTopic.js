/**
 * @brief Checks if a topic exists with the title 'topicName'
 * @param {*} topicName - Name of topic to check existance of.
 * @returns True if the topic does not exist.
 */
export async function uniqueTopic(topicName) {
    let encodedTopic = "";
    let existsTopic;
    
    // Encode username for existance check
    encodedTopic = encodeURI(topicName);
    
    // Checks the database if the username is taken
    existsTopic = await fetch(`${window.MyAppGlobals.serverURL}checkTopicExists?topic=${encodedTopic}`)
      .then( Response => {
        return Response.text();
      })
      .catch( Response => {
        console.error(Response);
    });
    
    return existsTopic === "false";
  }