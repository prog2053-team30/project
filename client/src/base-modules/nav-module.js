import { LitElement, html } from 'lit-element';
import { navStyle } from '../moduleStyle.js';
import { serverFetch } from '../scripts/serverFetch.js';

export class NavModule extends LitElement {
  static get styles() {
    return navStyle;
  }
  
  constructor() {
    super();
  }
  
  render() {
    return html`
    <div class="col-lg-10 col-md-12">
      <nav class="navbar">
        <!-- TODO: Add navbar toggler -->
        <a class="nav-link flex-fill text-center" href="/">Home</a>
        <a class="nav-link flex-fill text-center" href="/topic/all.html">All topics</a>
        <a class="nav-link flex-fill text-center" href="/posts.html">All posts</a>
        <a class="nav-link flex-fill text-center" href="/profile.html">Profile</a>
        <button type="button" class="btn nav-link flex-fill text-center" 
                @click="${this._logout}">
          Log out
        </button>
        <slot></slot>
      </nav>
    </div>
    `;
  }
  
  _logout() {
    const postLogoutURL = new URL('welcome.html', window.MyAppGlobals.clientURL);
    serverFetch('logout', {}, true).then( Response => {
      if ( Response.status === 200 ) {
        console.debug("Session destroyed");
        window.open(postLogoutURL, "_self");
      } else {
        console.warn(Response);
      }
    })
    .catch( Response => {
      console.error(Response);
    });
  }
}

window.customElements.define('nav-module', NavModule);