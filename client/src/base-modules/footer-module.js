import { LitElement, html } from 'lit-element';
import { footerStyle } from '../moduleStyle.js';

export class FooterModule extends LitElement {
  static get styles() {
    return footerStyle;
  }
  
  static get properties() {
    return {
      staticData: {type: Object},
      footerContent: {type: String}
    }
  }
  
  constructor() {
    super();
    
    this.staticData = {};
    this.footerContent = "";
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.staticData = window.MyAppGlobals.staticInfoData.footer;
      this.footerContent = this.staticData.copyright;
    }).catch( result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <footer class="col col-lg-10 col-md-12 pt-3">
      <p class="text-center">${this.footerContent}</p>
    </footer>
    `;
  }
}

window.customElements.define('footer-module', FooterModule);