import { LitElement, html } from 'lit-element';
import { headerStyle } from '/src/moduleStyle.js';

export class HeaderModule extends LitElement {
  static get styles() {
    return headerStyle;
  }
  
  static get properties() {
    return {
      staticData: {type: Object},
      headerText: {type: String},
      headerLogo: {type: String}
    };
  }
  
  constructor() {
    super();
    
    this.staticData = {};
    this.headerText = "";
    this.headerLogo = "";
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.staticData = window.MyAppGlobals.staticInfoData.header;
      this.headerText = this.staticData.title;
      this.headerLogo = this.staticData.logoPath;
    })
    .catch(result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <header class="col col-lg-10 col-md-12">
      <!-- TODO: Add logo? -->
      <h1>${ this.headerText }</h1>
      <slot></slot>
    </header>
    `;
  }
}

window.customElements.define('header-module', HeaderModule);