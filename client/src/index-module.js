import { LitElement, html } from 'lit-element';
import { contentDisplay } from './moduleStyle';
import './topic-modules/topic-list-module.js';

export class IndexModule extends LitElement {
  static get styles() {
    return [contentDisplay];
  }
  
  static get properties() {
    return {
      pageData: {type: Object},
      title: {type: String},
      description: {type: String},
    };
  }
  
  constructor() {
    super();
    this.pageData = "";
    this.title = "";
    this.description = "";
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      this.title = this.pageData.mainpage.descriptionTitle;
      this.description = this.pageData.mainpage.description;
      
      document.title = "Home" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <div class="row">
      <aside class="col-12">
        <h2>${ this.title }</h2>
        <p> ${ this.description } </p>
      </aside>
      <!-- Topics here -->
      <topic-list-module class="col-12">
      </topic-list-module>
    </div>
    `;
  }
}

window.customElements.define('index-module', IndexModule);