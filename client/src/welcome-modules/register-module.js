import { LitElement, html } from 'lit-element';
import { blockDisplay } from '../moduleStyle.js';
import { serverFormData } from '../scripts/serverFormData.js';
import { serverLogin } from '../scripts/serverLogin.js';
import { uniqueUsername } from '../scripts/uniqueUsername.js';

export class RegisterModule extends LitElement {
  static get styles() {
    return blockDisplay;
  }
  
  constructor() {
    super();
  }
  
  firstUpdated() {
    let registerForm = this.shadowRoot.querySelector(`#register-form`);
    // Disable default submit event
    registerForm.addEventListener('submit', (e) => {
      e.preventDefault();
      
      this._register(registerForm);
    });
  }
  
  render() {
    const USRNM = {
      MINLEN: window.MyAppGlobals.username.minlength,
      MAXLEN: window.MyAppGlobals.username.maxlength,
      VALCHAR: window.MyAppGlobals.username.validCharacters
    }
    const PSWD = {
      MINLEN: window.MyAppGlobals.password.minlength,
      MAXLEN: window.MyAppGlobals.password.maxlength,
      VALCHAR: window.MyAppGlobals.password.validCharacters
    }
    
    return html`
    <form id="register-form">
      <div class="form-group">
        <label for="usernameRegister">Username</label>
        <input type="text" 
               name="username"
               id="usernameRegister" 
               class="form-control needs-validation"
               minlength="${USRNM.MINLEN}"
               maxlength="${USRNM.MAXLEN}"
               pattern="[${USRNM.VALCHAR}]{${USRNM.MINLEN},${USRNM.MAXLEN}}"
               required>
        <small id="usernameRegisterHelp" class="form-text">
          Your username must be 3-40 characters long, 
          and only contain letters (a-z or A-Z) and numbers.
        </small>
      </div>
      <div class="form-group">
        <label for="passwordRegister">Password</label>
        <input type="password" 
               name="password"
               id="passwordRegister" 
               class="form-control needs-validation"
               minlength="${PSWD.MINLEN}"
               maxlength="${PSWD.MAXLEN}"
               pattern="[${PSWD.VALCHAR}]{${PSWD.MINLEN},${PSWD.MAXLEN}}"
               required>
        <small class="form-text">
          Your password must be 8-50 characters long, and can contain letters, 
          numbers, spaces and 
          <abbr title="${`!"#$%&'()*+´-./:;<=>?@[\\]^_{|}~`}"
          class="font-italic"
          >other printable ASCII characters</abbr>
        </small>
      </div>
      <div class="form-group">
        <label for="passwordConfirm">Confirm password</label>
        <input type="password" 
               name="passwordConfirm"
               id="passwordConfirm" 
               class="form-control"
               required>
               <!-- TODO: Create custom validity for matching passwords -->
      </div>
      <button type="submit" class="btn btn-secondary">
        Register
      </button>
    </form>
    `;
  }
  
  async _register(form) {
    const data = new FormData(form);
    
    let formContent = {};
    let password = {
      match: false,
      valid() {
        return this.match;
      }
    };
    let username = {
      unique: false,
      valid() {
        return this.unique;
      }
    };
    
    // Extracts the values from FormData
    for(let pair of data.entries()) {
      formContent[pair[0]] = pair[1].toString();
    }
    
    //TODO: Exit registration process if this does not match.
    password.match = this.passwordMatch(formContent.password, 
                                        formContent.passwordConfirm);
    
    //TODO: Exit registration process if the username is not unique
    username.unique = await uniqueUsername(formContent.username);
    
    // IF the username is unique AND the passwords match, send to server.
    if (username.valid() && password.valid()) {
      serverFormData('register', data, true).then( Response => {
        //TODO: Handle status codes
        if ( Response.status === 200 ) {
          serverLogin(data);
        } else {
          console.warn(Response);
        }
      }).catch( Response => {
        console.error(Response);
      });
    }
  }
  
  /**
   * Checks if the two strings are equal.
   * 
   * @param {*} passwordRegister 
   * @param {*} passwordConfirm 
   * @return bool - True if the passwords are equal.
   */
  passwordMatch(password, passwordConfirm) {
    return (password === passwordConfirm);
  }
}

window.customElements.define('register-module', RegisterModule);