import { LitElement, html } from 'lit-element';
import { flexDisplay } from '../moduleStyle.js';
import './login-module.js';
import './register-module.js';

export class WelcomeModule extends LitElement {
  static get styles() {
    return flexDisplay;
  }
  
  static get properties() {
    return {
      pageData: {type: Object},
    }
  }
  
  constructor() {
    super();
    this.pageData = {};
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "Welcome" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <div class="row align-self-center">
      <div class="col-12 text-center">
        <h2>Authentication required</h2>
        <p>
          To access this site you have to be logged in. <br>
          Log in with your username and password or register an account.
        </p>
      </div>
      <login-module class="col-md-6 col-sm-12"></login-module>
      <register-module class="col-md-6 col-sm-12"></register-module>
    </div>
    `;
  }
}

window.customElements.define('welcome-module', WelcomeModule)