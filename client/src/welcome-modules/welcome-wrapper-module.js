import { LitElement, html } from 'lit-element';
import { fetchStatic } from '/src/scripts/fetchStatic.js';
import { wrapperStyle } from '/src/moduleStyle.js';
import '../base-modules/footer-module.js';
import '../base-modules/header-module.js';

export class WelcomeWrapperModule extends LitElement {
  static get styles() {
    return wrapperStyle;
  }
  
  constructor() {
    super();
    
    // Start the promise for fetching the static site info
    window.MyAppGlobals.staticInfoPromise = fetchStatic();
    
    // Get the static site info to be used in sub-modules
    window.MyAppGlobals.staticInfoPromise.then(result => {
      window.MyAppGlobals.staticInfoData = result;
    })
    .catch(result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <header-module class="row justify-content-center"></header-module>
    <main class="row justify-content-center">
      <div class="col-xl-8 col-lg-10 col-md-12 pb-3">
        <slot></slot>
      </div>
    </main>
    <footer-module class="row justify-content-center"></footer-module>
    `;
  }
}

window.customElements.define('welcome-wrapper-module', WelcomeWrapperModule);