import { LitElement, html } from 'lit-element';
import { blockDisplay } from '../moduleStyle.js';
import { serverLogin } from '../scripts/serverLogin.js';

export class LoginModule extends LitElement {
  static get styles() {
    return blockDisplay;
  }
  
  constructor() {
    super();
  }
  
  firstUpdated() {
    let loginForm = this.shadowRoot.querySelector(`#login-form`);
    // Disable default submit event
    loginForm.addEventListener('submit', (e) => {
      e.preventDefault();
      
      this._login(loginForm);
    });
  }
  
  render() {
    const USRNM = {
      MINLEN: window.MyAppGlobals.username.minlength,
      MAXLEN: window.MyAppGlobals.username.maxlength,
      VALCHAR: window.MyAppGlobals.username.validCharacters
    }
    const PSWD = {
      MINLEN: window.MyAppGlobals.password.minlength,
      MAXLEN: window.MyAppGlobals.password.maxlength,
      VALCHAR: window.MyAppGlobals.password.validCharacters
    }
    
    return html`
    <form id="login-form">
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" 
               name="username" 
               id="username" 
               class="form-control needs-validation"
               minlength="${USRNM.MINLEN}"
               maxlength="${USRNM.MAXLEN}"
               pattern="[${USRNM.VALCHAR}]{${USRNM.MINLEN},${USRNM.MAXLEN}}"
               required>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" 
               name="password"
               id="password" 
               class="form-control needs-validation"
               minlength="${PSWD.MINLEN}"
               maxlength="${PSWD.MAXLEN}"
               pattern="[${PSWD.VALCHAR}]{${PSWD.MINLEN},${PSWD.MAXLEN}}"
               required>
      </div>
      <button id="submit" type="submit" class="btn btn-primary">
        Log in
      </button>
    </form>
    `;
  }
  
  _login(form) {
    const data = new FormData(form);
    
    serverLogin(data);
  }
}

window.customElements.define("login-module", LoginModule);