/* Module for single user listing */
import { LitElement, html } from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';
import '../moderator-modules/single-user-module.js';
import '../generic-modules/pagination-module.js';

export class UserListModule extends LitElement {
  static get properties() {
    return {
      userList: {type: Object},
    }
  }

  constructor() {
    super();
    
    let params = new URL(window.location).searchParams;
    
    
    this.userList = {
      total: 0,
      ref: 0,
      increment: 10,
      users: []
    };
    
    if (params.has('user-ref')) {
      this.userList.ref = params.get('user-ref');
    }

    const userFetch = serverFetch("modPageUserList", {ref: this.userList.ref, 
                                                      increment: this.userList.increment});
    userFetch.then( Response => {
      this.userList.total = Response.UserCount;
      this.userList.ref = Response.ref;
      this.userList.users = Response.users;
      this.requestUpdate();
    })
    .catch( Response => {
      console.error(Response);
    });
  }
  
  
  render() {
    return html`
    <div class="card">
      <div class="card-header">
        <h5 class="card-title mb-0">User list</h5>
      </div>
      <div class="card-body p-0">
        <!-- TODO: Searchable? -->
        <!-- TODO: Show all users username, nr. post, comments, date joined, ban button, user type, file person bootstrap-->
        ${this.userList.users.map(i=>{
          return html`
        <single-user-module id="user_${i.uid}"
          uid = "${i.uid}"
          username = "${i.username}"
          post = "${i.post_count}"
          comment = "${i.comment_count}"
          date = "${i.userCreated}"
          usertype = "${i.userType}">
        </single-user-module>
        `;
      })}
      </div>

      ${this.userList.total > this.userList.increment?
        html`
          <div class="card-footer p-0">
            <pagination-module
              total="${this.userList.total}"
              refprefix="user"
              ref="${this.userList.ref}"
              increment="${this.userList.increment}">
            </pagination-module>
          </div>
        `:""
      }
    </div>
    `;
  }
}

customElements.define("user-list-module", UserListModule);