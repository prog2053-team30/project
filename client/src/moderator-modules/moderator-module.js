/* Main moderator module */
import { LitElement, html } from 'lit-element';
import { contentDisplay, columnStyle } from '../moduleStyle.js';
import './removed-items-module.js';
import './user-list-module.js';

export class ModeratorModule extends LitElement {
  static get styles() {
    return [contentDisplay, columnStyle];
  }
  
  static get properties() {
    return {
      pageData: {type: Object}
    }
  }
  
  constructor() {
    super();
    
    this.pageData = {};
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "Moderator" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <h2>Moderatator tools</h2>
    <div class="card-columns">
      <user-list-module></user-list-module>
      <removed-items-module></removed-items-module>
    </div>
    `;
  }
}

customElements.define("moderator-module", ModeratorModule);