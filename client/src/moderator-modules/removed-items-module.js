import { LitElement, html } from 'lit-element';
import { serverFetch } from "../scripts/serverFetch.js";
import '../generic-modules/pagination-module.js';

export class RemovedItemsModule extends LitElement {
  static get properties() {
    return {
      items: {type: Object}
    }
  }
  
  constructor() {
    super();
    
    let params = new URL(window.location).searchParams;
    
    this.items = {
      total: 0,
      increment: 10,
      ref: 0,
      refPrefix: "item",
      item: []
    }
    
    if ( params.has('rm-ref') ) {
      this.items.ref = params.get('rm-ref');
    }
    
    const removedFetch = serverFetch("modBlockedContentList", 
                                    {ref: this.items.ref,
                                     increment: this.items.ref});
    removedFetch.then( Response => {
      Response.users.forEach(element => {
        element.type = "user";
        this.items.item.push(element);
      });
      Response.posts.forEach(element => {
        element.type = "post";
        this.items.item.push(element);
      });
      Response.comments.forEach(element => {
        element.type = "comment";
        this.items.item.push(element);
      });
      this.items.total = this.items.item.length;
      this.requestUpdate();
    })
    .catch( Response => {
      console.error(Response);
    });
    this.items.item.slice(this.items.ref, (this.items.ref + (this.items.increment - 1)));
  }
  
  render() {
    return html`
    <!-- 
      Due to a bug with cards of the same heigth in a card-column, 
      this card has to have a different bottom margin than the rest.
      Removing this specific margin could BREAK how cards are displayed
      on some or ALL browser. DO NOT REMOVE.
    -->
    <div class="card" style="margin-bottom: 0.74rem">
      <div class="card-header">
        <h5 class="card-title mb-0">Removed items</h5>
      </div>
      <div class="card-body p-0">
        ${this.items.item.map( i => {
          if (i.type === "comment") {
            return this.renderComment(i);
          } else if (i.type === "post") {
            return this.renderPost(i);
          } else if (i.type === "user") {
            return this.renderUser(i);
          } else {
            console.error("Something went wrong rendering removed elements.");
          }
        })}
      </div>
      ${this.items.total > this.items.increment?
        html`
          <div class="card-footer">
          <pagination-module
              total="${this.items.total}"
              refprefix="rm"
              ref="${this.items.ref}"
              increment="${this.items.increment}">
            </pagination-module>
          </div>
        `:""
      }
    </div>
    `;
  }
  
  renderComment(data) {
    let toRender = {
      description: `Comment by ${data.username}`,
      type: data.type,
      id: data.cid,
      content: html`<b>Content:</b> ${data.comment}`,
      date: html`<b>Posted</b> ${new Date(data.date).toDateString()}`
    }
    return this.renderElement(toRender);
  }
  
  renderPost(data) {
    let toRender = {
      description: `Post by ${data.username}`,
      id: data.user_ID,
      type: data.type,
      title: html`<b>Title: </b>${data.title}`,
      content: html`<b>Content: </b>${data.content}`,
      date: html`<b>Posted</b> ${new Date(data.date).toDateString()}`
    }
    return this.renderElement(toRender);
  }
  
  renderUser(data) {
    let toRender = {
      description: `User: ${data.username}`,
      id: data.uid,
      date: html`<b>Joined:</b> ${new Date(data.userCreated).toDateString()}`,
      type: data.type
    }
    return this.renderElement(toRender);
  }
  
  renderElement(data) {
    return html`
    <div class="card-header p-0 mb-1 border-0">
      <button class="btn btn-secondary w-100"
              type = "button"
              value = "#rem_${data.type}_${data.id}"
              @click="${this._toggle}">
        ${data.description}
      </button>
      <div id="rem_${data.type}_${data.id}" class="collapse" data-parent="#">
        <div class="card-body pt-1">
          ${data.title != undefined?
            html`<p class="m-0 pb-1">${data.title}</p>`:""
          }
          ${data.content != undefined?
            html`<p class="m-0 pb-1">${data.content}</p>`:""
          }
          <p class="m-0 pb-1">${data.date}</p>
        </div>
      </div>
    </div>
    `;
  }
  
  /* Disable shadow dom for this module */
  createRenderRoot() {
    return this;
  }
  
  _toggle(e) {
    let toggleElement = this.querySelector(`${e.target.value}`);
    toggleElement.classList.toggle('show')
  }
  
}

customElements.define('removed-items-module', RemovedItemsModule);