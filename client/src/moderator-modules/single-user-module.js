import { LitElement, html } from 'lit-element';

export class SingleUserModule extends LitElement {
  static get properties() {
    return {
      uid: {type: Number},
      username: {type: String},
      usertype: {type: String},
      post: {type: Number},
      comment: {type: Number},
      date: {type: String},
      userTypeImg: {type: String},
      userDescription: {type: String}
    }
  }

  constructor()   {
    super();
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    
    this.userTypeImg = "/node_modules/bootstrap-icons/bootstrap-icons.svg";
    this.userDescription = "Usertype: "
  }
    
  render() {
    switch(this.usertype) {
      case "admin":
        this.userTypeImg = this.userTypeImg + "#tools";
        this.userDescription = this.userDescription + "Administrator";
        break;
      case "moderator":
        this.userTypeImg = this.userTypeImg + "#wrench";
        this.userDescription = this.userDescription + "Moderator";
        break;
      case "user":
        this.userTypeImg = this.userTypeImg + "#person-fill";
        this.userDescription = this.userDescription + "User";
        break;
    };
    
    return html`
    <div class="card-header mb-1 p-0 border-0">
      <div class="input-group d-flex justify-content-between">
        <div class="input-group-prepend flex-fill">
          <button type="button" 
            class="btn btn-secondary rounded-0 text-left w-100"
            @click="${this._toggle}">
            ${this.username}
          </button>
        </div>
        <div class="input-group-append">
          <span class="input-group-text rounded-0">
            <svg class="bi" width="1em" height="1em" fill="currentcolor" 
                 data-toggle="tooltip" title="${this.userDescription}">
              <use href="${this.userTypeImg}">
            </svg>
          </span>
        </div>
      </div>
      <div id="${this.username}" class="collapse" data-parent="#user_${this.uid}">
        <div class="card-body py-2">
          <div class="m-0 d-flex justify-content-between">
            <small class="m-0 d-inline">Joined:</small>
            <small class="m-0 d-inline">${(new Date(this.date)).toDateString()}</small>
          </div>
          <div class="m-0 d-flex justify-content-between">
            <small class="m-0 d-inline">Posts:</small>
            <small class="m-0 d-inline">${this.post}</small>
          </div>
          <div class="m-0 d-flex justify-content-between">
            <small class="m-0 d-inline">Comments:</small>
            <small class="m-0 d-inline">${this.comment}</small>
          </div>
        </div>
      </div>
    </div>
    `;
  }
  
  createRenderRoot() {
    return this;
  }
  
  _toggle(e) {
    let toggleElement = this.querySelector(`#${this.username}`);
    toggleElement.classList.toggle('show');
  }
}
customElements.define('single-user-module', SingleUserModule);

