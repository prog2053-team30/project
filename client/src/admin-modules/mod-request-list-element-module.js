import { LitElement, html } from 'lit-element';
import { serverFetch }  from '../scripts/serverFetch.js';

export class ModRequestListElementModule extends LitElement {
  static get properties() {
    return {
      uid: {type: Number},
      username: {type: String},
      date: {type: String},
      posts: {type: Number},
      comments: {type: Number}
    }
  }
  
  constructor() {
    super();
  }
  
  render() {
    return html`
    <div class="card-header p-0 border-0">
      <div class="d-flex justify-content-between">
        <div class="btn-group btn-group-justified w-100" role="group">
          <button class="btn btn-secondary w-100 text-left" 
                  type="button" 
                  data-toggle="collapse" 
                  data-target="#${this.username}"
                  @click="${this.toggle}">
            ${this.username}
          </button>
          <button type="button" class="btn btn-success" @click="${this._approveModReq}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#check-circle" />
            </svg>
          </button>
          <button type="button" class="btn btn-danger" @click="${this._denyModReq}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#x-circle" />
            </svg>
          </button>
        </div>
      </div>
      <div id="${this.username}" class="collapse" data-parent="#mod-req">
        <div class="card-body pt-1">
          <div class="m-0 d-flex justify-content-between">
            <p class="m-0 d-inline">Joined:</p>
            <p class="m-0 d-inline">${this.date}</p>
          </div>
          <div class="m-0 d-flex justify-content-between">
            <p class="m-0 d-inline">Posts:</p>
            <p class="m-0 d-inline">${this.posts}</p>
          </div>
          <div class="m-0 d-flex justify-content-between">
            <p class="m-0 d-inline">Comments:</p>
            <p class="m-0 d-inline">${this.comments}</p>
          </div>
        </div>
      </div>
    </div>
    `;
  }
  
  createRenderRoot() {
    return this;
  }
  
  toggle(e) {
    let toggleElement = this.querySelector(`#${this.username}`);
    toggleElement.classList.toggle('show');
  }
  
  _approveModReq(e) {
    console.debug(`Approve mod req for user ${this.username} with uid ${this.uid}`);
    const modReq = serverFetch("modRequestResponse", {uid: this.uid, approve: true}, true);
    modReq.then( Response => {
      if ( Response.status === 200) {
        location.reload();
      } else {
        console.warn(Response);
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }
  
  _denyModReq(e) {
    console.debug(`Deny mod req for user ${this.username} with uid ${this.uid}`);
    const modReq = serverFetch("modRequestResponse", {uid: this.uid, approve: false}, true);
    modReq.then( Response => {
      if (Response.status === 200) {
        location.reload();
      } else {
        console.warn(Response);
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }
}

customElements.define("mod-request-list-element-module", ModRequestListElementModule);