/* Main admin module */
import { LitElement, html } from 'lit-element';
import { contentDisplay, columnStyle } from '../moduleStyle.js';
import '../moderator-modules/user-list-module.js';
import '../moderator-modules/removed-items-module.js';
import './mod-list-module.js';
import './mod-request-list-module.js';

export class AdminModule extends LitElement {
  static get styles() {
    return [contentDisplay, columnStyle];
  }
  
  static get properties() {
    return {
      pageData: {type: Object},
    }
  }
  
  constructor() {
    super();
    
    this.pageData = {};
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "Administrator" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    //TODO: Only render if logged in and correct permissions
    return html`
    <h2>Administrator tools</h2>
    <div class="card-columns">
      <mod-request-list-module></mod-request-list-module>
      <mod-list-module></mod-list-module>
      <user-list-module></user-list-module>
      <removed-items-module></removed-items-module>
    </div>
    `;
  }
}

customElements.define("admin-module", AdminModule);