import { LitElement, html } from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';

import './mod-request-list-element-module.js';

export class ModRequestListModule extends LitElement {
  
  static get properties() {
    return {
      modRequests: {type: Object}
    }
  }
  
  constructor() {
    super();
    
    let params = new URL(window.location).searchParams;
    
    this.modRequests = {
      total: 0,
      ref: 0,
      increment: 10,
      request: [
        {
          uid: 0,
          username: "",
          userCreated: 0,
          posts: 0,
          comments: 0
        }
      ]
    };
    
    if ( params.has('modreq-ref') ) {
      this.modRequests.ref = params.get('modreq-ref');
    }
    
    const requestFetch = serverFetch("modRequestList", 
                                      {ref: this.modRequests.ref, 
                                       increment: this.modRequests.increment});
    requestFetch.then( Response => {
      this.modRequests.total = Response.total;
      this.modRequests.request = Response.users;
      this.requestUpdate();
    })
    .catch( Response => {
      //TODO: Display error message to user.
      console.error(Response);
    })
  }
  
  render() {
    return html`
    <div class="card">
      <div class="card-header">
        <h5 class="card-title mb-0">Mod requests</h5>
      </div>
      <div class="accordion" id="mod-req">
        ${this.modRequests.total === 0?
          html`
            <span class="d-block badge badge-secondary mx-auto">There are no requests to become moderator at the moment.</span>
          `:""
        }
        ${this.modRequests.request.map( i => {
          return html`
            <mod-request-list-element-module 
              class="card m-0"
              uid="${i.uid}"
              username="${i.username}"
              date="${(new Date(i.userCreated)).toDateString()}"
              posts="${i.posts}"
              comments="${i.comments}">
            </mod-request-list-element-module>
          `
        })}
      </div>
      ${this.modRequests.total > this.modRequests.increment?
        html`
          <div class="card-footer">
            <pagination-module
              total="${this.modRequests.total}"
              refprefix="modreq"
              ref="${this.modRequests.ref}"
              increment="${this.modRequests.increment}">
            </pagination-module>
          </div>
        `:""
      }
    </div>
    `;
  }
}

customElements.define('mod-request-list-module', ModRequestListModule);