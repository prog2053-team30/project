import { LitElement, html } from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';
import '../moderator-modules/single-user-module.js';

export class ModListModule extends LitElement {

  static get properties() {
    return {
      modList: {type: Object},
    }
  }
  
  constructor() {
    super();
    
    let params = new URL(window.location).searchParams;
    
    this.modList = {
      total: 0,
      ref: 0,
      increment: 10,
      users: []
    };
    
    if ( params.has('mod-ref') ) {
      this.modList.ref = params.get('mod-ref');
    }

    const userFetch = serverFetch("adminPageModList", {ref: 0, increment: 10});
    userFetch.then( Response => {
      this.modList.total = Response.UserCount;
      this.modList.users = Response.users;
      this.requestUpdate();
    })
    .catch( Response => {
      console.error(Response);
    });
  }
  
  render() {
    return html`
    <div class="card">
      <div class="card-header">
        <h5 class="card-title mb-0">Moderators</h5>
      </div>
      <div class="card-body p-0">
        <!-- TODO: List all moderators -->
        ${this.modList.users.map(i=>{
          return html`
        <single-user-module 
          uid = "${i.uid}"
          username = "${i.username}"
          post = "${i.post_count}"
          comment = "${i.comment_count}"
          date = "${i.userCreated}"
          usertype = "${i.userType}">
        </single-user-module>
        `;
      })}
      </div>
      ${this.modList.total > this.modList.increment?
        html`
          <div class="card-footer">
            <pagination-module
              total="${this.modList.total}"
              refprefix="mod"
              ref="${this.modList.ref}"
              increment="${this.modList.increment}">
            </pagination-module>
          </div>
        `:""
      }
    </div>
    `;
  }
}
customElements.define('mod-list-module', ModListModule);