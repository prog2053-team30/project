import { LitElement, html } from 'lit-element';
import '/src/post-modules/single-post-module.js';
import { serverFetch } from '/src/scripts/serverFetch.js';

export class SearchResultModule extends LitElement {
  static get styles() {
    //return
  }
  
  static get properties() {
    return {
      pageData: {type: Object},
      query: {type: String},
      searchPosts: {type: Object},
    }
  }
  
  constructor() {
    super();

    let params = (new URL(document.location)).searchParams;
    
    this.query = params.get('q');    //TODO send searched keyword to db
    this.pageData = {};

    this.post = {
      id: params.get('pid'),
      title: "",
      date: "",
      dateString: "",
      score: "",
      content: ""
    };

    this.topic = {
      id: params.get('tid'),
      title: "",
      description: "",
      date: ""
    };

    this.searchPosts = {
      total: 0,
      ref: 0,
      post: []
    }

    const searchPosts = serverFetch("searchPosts", {search: this.query, ref: 0, increment: 10});
    searchPosts.then( Response => {
      this.searchPosts = Response;
      
    })
    .catch ( Response => {
      console.error(Response);
    })
    
    window.MyAppGlobals.staticInfoPromise.then( () =>{
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "Search" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <h2>Searching:</h2>
    <p>
      Showing results for: ${this.query}
    </p>
    <div class="col-12">
      <h3>Posts:</h3>
      ${this.searchPosts.post.map(i => {
        let postURL = new URL('/topic/post/index.html', window.MyAppGlobals.clientURL);
        postURL.search = `tid=${i.topic}&pid=${i.pid}`;
        return html`
        <single-post-module class="row mb-1"
          posturl="${postURL.toString()}"
          title="${i.title}"
          username="${i.username}"
          postdate="${(new Date(i.date)).toDateString()}"
          score="${Number(i.vote_score)}"
          comments="${Number(i.comment_count)}">
        </single-post-module>
        `;
      })}
    </div>
    `;
  }
  
}

window.customElements.define('search-result-module', SearchResultModule);