/* Single topic list element */ 
import { LitElement, html } from 'lit-element';
import { partStyle, singleTopicListStyle } from '../moduleStyle.js';

export class SingleTopicListModule extends LitElement {
  static get styles() {
    return [singleTopicListStyle,partStyle];
  }
  
  static get properties() {
    return {
      topicNr: { type: Number },
      topicTitle: { type: String },
      topicURL: { type: String },
      numPosts: { type: Number },
      createDate: { type: String}
    }
  }
  
  constructor() {
    super();
  }
  
  render() {
    return html`
    <div class="col-12 d-flex part-style my-1">
      <div class="topicNr text-center">
        <span></span>
      </div>
      <div class="topicDetails flex-fill position-relative">
        <h5 class="d-block text-truncate">
          <a href="${this.topicURL}">${this.topicTitle}</a>
        </h5>
        <div class="d-flex justify-content-between">
          <p class="d-inline m-0">
            Posts: ${this.numPosts}
          </p>
          <p class="d-inline m-0">
            Topic created: ${this.createDate}
          </p>
        </div>
      </div>
    </div>
    `;
  }
}

window.customElements.define("single-topic-list-module", SingleTopicListModule)