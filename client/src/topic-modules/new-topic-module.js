import { LitElement, html } from 'lit-element';

import { blockDisplay } from '../moduleStyle.js';
import { serverFormData } from '../scripts/serverFormData.js';
import { uniqueTopic } from "../scripts/uniqueTopic.js";

export class newTopic extends LitElement{
  static get styles() {
    return blockDisplay;
  }

  static get properties() {
    return {
      pageData: {type: Object},
    };
  }

  constructor() {
    super();

    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "New topic" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
    
  }
  
  firstUpdated() {
    const topicForm = this.shadowRoot.querySelector("form");
    
    topicForm.addEventListener('submit', (e) => {
      e.preventDefault();
      
      this._createTopic(topicForm);
    });
  }
  
  render() {
    return html`
    <form>
      <h2> Create topic </h2>
      <div class="form-group">
        <label for="topic"> Topic name </label>
        <!-- TODO:add rows and cols -->
        <input type="text" 
               id="topic" 
               name="topic" 
               class="form-control" 
               minlength="3"
               maxlength="80"
               placeholder="Required"
               required>
      </div>
      <div class="form-group">
        <label for="description"> Topic description </label>
        <!-- TODO:add rows and cols -->
        <textarea id="description"
                  class="form-control" 
                  name="description"
                  placeholder="(Recommended)"
                  minlength="3"
                  maxlength="280"></textarea>
      </div>
      <button type="submit" 
              class="btn btn-primary">
        Create topic
      </button>
    </form>
    `;
  }
  
  async _createTopic(form) {
    const data = new FormData(form);
    
    let newTopicUrl = new URL('topic/index.html', window.MyAppGlobals.clientURL);
    let formContent = {};
    let topic = {
      title: {
        unique: false
      },
      valid() {
        return (this.title.unique);
      }
    }
    
    // Extract the values from FormData
    for(let pair of data.entries()) {
      formContent[pair[0]] = pair[1].toString();
    }
    
    // Verify content of form
    topic.title.unique = await uniqueTopic(formContent.title);
    
    // Only create a topic if the formContent passes the tests
    if (topic.valid()) {
      serverFormData("newTopic", data).then( Response => {
        if ( Response.tid > 0 ) {
          newTopicUrl.searchParams.append('tid', Response.tid);
          window.open(newTopicUrl, "_self");
        } else {
          console.error("There was an issue on the server");
        }
      })
      .catch ( Response => {
        //TODO: Display error message to user
        console.error(Response);
      })
    }
  }
}

customElements.define('new-topic-module', newTopic);