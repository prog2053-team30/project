import { LitElement, html } from 'lit-element';

import { blockDisplay } from '../moduleStyle.js';
import { serverFetch } from '../scripts/serverFetch.js';

import '../post-modules/post-list-module.js';
import '../post-modules/single-post-module.js';
import '../generic-modules/pagination-module.js';
import '../generic-modules/sort-module.js';

export class topicPage extends LitElement {
  static get styles() {
    return blockDisplay;
  }
  
  static get properties() {
    return {
      tid: {type: Number},
      pageData: {type: Object},
      topic: {type: Object}
    };
  }
  
  constructor() {
    super();
    
    // Get the URL parameters
    let params = (new URL(window.location)).searchParams;
    // What to sort by.
    let sortBy;
    
    // Topic object 'template'
    this.topic = {
      id: params.get('tid'),  // Topic ID
      title: "",        // Topic title
      description: "",  // Description of topic (optional)
      date: "",         // When the topic was created
      posts: {
        increment: 10,  // Number of posts per page of pagination
        total: 0, // Total number of posts in topic, used for pagination
        ref: 0,   // Where the first displayed post one the page is in the array of posts
        sort: "date",   // What to default sort the posts by, date or score
        post: [{  // Array of objects, each object is a post, will be fully replaced by server.
          pid: 0,       // Post ID
          title: "",    // Title of post
          username: "", // User that added the post
          postdate: "", // When the post was added
          vote_score: 0,     // Sum of up and downvotes
          comment_count: 0   // Number of comments on the post
        }]
      }
    };
    
    // If a referanse value for pagination is stored in URL, use that.
    if (params.has('ref')) {
      this.topic.posts.ref = params.get('ref');
    }
    
    // If a sort method is stored in the URL, use that.
    if (params.has('sort')) {
      sortBy = params.get('sort');
    } else {
      sortBy = this.topic.posts.sort;
    }
    
    // Fetch all data and posts related to this topic
    const topicData = serverFetch("topic", {
      tid: Number(this.topic.id),
      sort: sortBy,
      ref: this.topic.posts.ref,
      increment: this.topic.posts.increment
    });
    
    // Store data fetched once ready
    topicData.then( Response => {
      this.topic.title = Response.title;
      this.topic.description = Response.description;
      this.topic.date = Response.date;
      
      this.topic.posts.total = Response.posts.total;
      this.topic.posts.post = Response.posts.post;
      this.requestUpdate();
    })
    .catch( Response => {
      console.error(Response);
    });
  
   // Fetch some metadata for this site
   window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "Topic" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    let newPostURL = new URL("/topic/post/new.html", window.MyAppGlobals.clientURL);
    // Append the topic ID to the URL of new post creation
    newPostURL.search = `tid=${this.topic.id}`;
    
    return html`
    <h2>${this.topic.title}</h2>
    <p>${this.topic.description}</p>
    <!-- TODO delete button for owner, mods, admins -->
    <sort-module defaultsort="${this.topic.posts.sort}">
    </sort-module>
    <div class="col-12">
      ${this.topic.posts.post.map( i => {
        let postURL = new URL('/topic/post/index.html', window.MyAppGlobals.clientURL);
        // Append topic ID and post ID to the posts URL
        postURL.search = `tid=${this.topic.id}&pid=${i.pid}`;
        
        return html`
        <single-post-module class="row"
          posturl="${postURL.toString()}"
          title="${i.title}"
          username="${i.username}"
          postdate="${(new Date(i.date)).toDateString()}"
          score="${Number(i.vote_score)}"
          comments="${Number(i.comment_count)}">
        </single-post-module>
        `;
      })}
      ${this.topic.posts.total > this.topic.posts.increment?
        html`
          <pagination-module class="row"
            total="${this.topic.posts.total}"
            ref="${this.topic.posts.ref}"
            increment="${this.topic.posts.increment}">
          </pagination-module>
        `:""
      }
    </div>
    <div>
      <a href="${newPostURL.toString()}" class="btn btn-primary">Create post</a>
    </div>
    `;
  }
}

window.customElements.define('topic-page-module', topicPage);