import { LitElement, html } from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';

import '../topic-modules/single-topic-list-module.js';
import '../generic-modules/pagination-module.js';

export class TopicListModule extends LitElement {
  static get properties() {
    return {
      topics: {type: Object}
    }
  }
 
  constructor() {
    super();
    
    let params = (new URL(window.location)).searchParams;
    
    this.topics = {
      total: 0,
      ref: 0,
      increment: 10,
      topic: {}
    };
    
    if (params.has('ref')) {
      this.topics.ref = params.get('ref');
    }
    
    const topicsData = serverFetch("topics", {ref: this.topics.ref, 
                                              increment: this.topics.increment});
    topicsData.then( Response => {
      this.topics.total = Response.total;
      this.topics.topic = Response.topic;
      this.requestUpdate();
    })
    .catch( Response => {
      console.error(Response);
    })
  }
  
  render() {
    return html`
    <div>
      <h3>Topics:</h3>
    </div>
    <div>
      ${this.topics.total > 0 ? 
          this.topics.topic.map(i => {
            let topicURL = new URL('/topic/index.html', window.MyAppGlobals.clientURL);
            topicURL.search = `tid=${i.topicID}`;
            
            return html`
              <single-topic-list-module class="row"
                topictitle="${i.title}"
                topicurl="${topicURL.toString()}"
                numposts="${i.post_count}"
                createdate="${(new Date(i.createDate)).toDateString()}">
              </single-topic-list-module>
            `
          })
        :html`<span>There does not seem to be anything here yet</span>`
      }
      ${(this.topics.total > this.topics.increment)?
        html`
        <pagination-module class="row"
          total="${this.topics.total}"
          ref="${this.topics.ref}"
          increment="${this.topics.increment}">
          </pagination-module>
        `:""
      }
      <a href="/topic/new.html" class="btn btn-primary">New topic</a>
    </div>
    `;
  }
}

window.customElements.define("topic-list-module", TopicListModule);