import { LitElement, html } from 'lit-element';
import '../topic-modules/topic-list-module.js';

export class AllTopicListModule extends LitElement {
  static get properties() {
    return {
      pageData: {type: Object}
    }
  }
  
  constructor()   {
    super();
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "All topics list" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render()    {
    return html`
    <div class="row">
      <topic-list-module class="col-12">
      </topic-list-module>
    </div> 
    `;
  }
}
customElements.define('all-topic-list-module', AllTopicListModule);