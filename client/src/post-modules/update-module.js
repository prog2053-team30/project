import { LitElement, html } from 'lit-element';
import { serverFormData } from '../scripts/serverFormData.js';
import { charCount } from '../scripts/charCount.js';

export class UpdateModule extends LitElement {    
  static get properties() {
    return {
      pid: {type: Number},
      title: {type: String},
      content: {type: String},
      post: {type: Object}
    }
  }

  constructor() {
    super();
    this.post = {
      minLength: 3,
      helpText: "",
      title: {
        maxLength: 200
      },
      body: {
        maxLength: 20000
      }
    }
  }

  firstUpdated() {
    // Extract post ID from URL
    this.pid = (new URL(window.location).searchParams).get('pid');
    const updatePostForm = this.querySelector('#update-post-form');

    this.post.helpText = `Please write at least ${this.post.minLength} characters.`;
    
    // Capture submit event
    updatePostForm.addEventListener('submit', (e) => {
      // Disable default submit action
      e.preventDefault();
      
      // Use custom action
      this._updatePost(updatePostForm);
    });
  }

  render() {
    return html`
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="updateModalLabel">Edit</h5>
            <button type="button" 
                    class="close" 
                    data-dismiss="modal" 
                    aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="update-post-form">
            <div class="modal-body">
              <div class="form-group">
                <label for="new-title" class="col-form-label">New title:</label>
                <input name="newTitle" 
                minlength="${this.post.minLength}"
                maxlength="${this.post.title.maxLength}"
                type="text" 
                class="form-control" 
                id="newTitle" 
                value="${this.title}" 
                @keyup="${this._elemGrab}"
                required>
                <small>${this.post.helpText}</small>
              </div>
              <div class="form-group">
                <label for="new-content" class="col-form-label">New content:</label>
                <textarea name="newContent" 
                minlength="${this.minLength}"
                maxlength="${this.maxLength}"
                class="form-control" 
                id="newContent"
                required 
                @keyup="${this._elemGrab}">${this.content}</textarea>
                <small></small>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" 
                      class="btn btn-secondary" 
                      data-dismiss="modal">
                Close
              </button>
              <button type="submit" 
                      class="btn btn-primary">
                Update post
              </button>
            </div>
          </form>
        </div>
      </div> 
    `;
    }

    /* Disables shadowRoot for this module */
  createRenderRoot() {
    return this;
  }

  _updatePost(form) {
    const data = new FormData(form);
    let updatedPostURL = new URL("topic/post/index.html", window.MyAppGlobals.clientURL);

    data.set('pid', this.pid);

    serverFormData("updatePost", data, true).then(Response => {
      if(Response.status===200) {
        window.location.reload();
      }else {
        console.error("something went wrong when updating post");
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }

  _elemGrab(e) {
    let inputNode = e.target;
    let outputNode = inputNode.nextElementSibling;
    
    charCount(inputNode, outputNode);
  }
}
customElements.define('update-module', UpdateModule);