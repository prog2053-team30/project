import { LitElement, html } from 'lit-element';
import { serverFormData } from '../scripts/serverFormData.js';
import { charCount } from '../scripts/charCount.js';

export class UpdateCommentModule extends LitElement {    
  static get properties() {
    return {
      cid: {type: Number},
      comment: {type: String}
    }
  }

  constructor() {
    super();
    this.minLength = 3;
    this.maxLength = 20000;
  }

  firstUpdated() {
    // Extract post ID from URL
    this.pid = (new URL(window.location).searchParams).get('pid');
    const updateCommentForm = this.querySelector('#update-comment-form');
    
    // Capture submit event
    updateCommentForm.addEventListener('submit', (e) => {
      // Disable default submit action
      e.preventDefault();
      
      // Use custom action
      this._updateComment(updateCommentForm);
    });
  }

  render() {
    return html`
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="updateModalLabel">Edit comment</h5>
            <button type="button" 
                    class="close" 
                    data-dismiss="modal" 
                    aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="update-comment-form">
            <div class="modal-body">
              <div class="form-group">
                <label for="new-comment" class="col-form-label">New comment:</label>
                <textarea name="newComment" 
                minlength="${this.minLength}"
                maxlength="${this.maxLength}"
                class="form-control" 
                id="newComment"
                required 
                @keyup="${this._elemGrab}">${this.comment}</textarea>
                <small></small>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" 
                      class="btn btn-secondary" 
                      data-dismiss="modal">
                Close
              </button>
              <button type="submit" 
                      class="btn btn-primary">
                Update comment
              </button>
            </div>
          </form>
        </div>
      </div> 
    `;
    }

    /* Disables shadowRoot for this module */
  createRenderRoot() {
    return this;
  }

  _updateComment(form) {
    const data = new FormData(form);
    let updatedCommentURL = new URL("topic/post/index.html", window.MyAppGlobals.clientURL);

    data.set('cid', this.cid);

    serverFormData("updateComment", data, true).then(Response => {
      if(Response.status===200) {
        window.location.reload();
      }else {
        console.error("something went wrong when updating comment");
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }

  _elemGrab(e) {
    let inputNode = e.target;
    let outputNode = inputNode.nextElementSibling;
    
    charCount(inputNode, outputNode);
  }
}
customElements.define('update-comment-module', UpdateCommentModule);