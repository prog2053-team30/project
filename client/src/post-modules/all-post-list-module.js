import { LitElement, html } from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';

import '../post-modules/single-post-module.js';
import '../generic-modules/pagination-module.js';
import '../generic-modules/sort-module.js';

export class allPostListModule extends LitElement {

  static get properties() {
    return {
      pageData: {type: Object},
      allPosts: {type: Object},
      pid: {type: Number},
      tid: {type: Number},
    }
  }
  
  constructor() {
    super();

    let params = (new URL(window.location)).searchParams;
    let sortBy;
    
    this.allPosts = {
      total: 0,
      ref: 0,
      increment: 10,
      post: []
    }
    
    if (params.has('ref')) {
      this.allPosts.ref = params.get('ref');
    }
    
    if (params.has('sort')) {
      sortBy = params.get('sort');
    } else {
      sortBy = "score";
    }

    const allPosts = serverFetch("allPosts", {ref: this.allPosts.ref,
                                              sort: sortBy,
                                              increment: this.allPosts.increment});
    allPosts.then( Response => {
      this.allPosts.total = Response.total;
      this.allPosts.post = Response.post;
      this.requestUpdate();
    })
    .catch ( Response => {
      console.error(Response);
    })

    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      
      document.title = "All posts list" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  render() {
    return html`
      <div>
        <h3>Posts:</h3>
      </div>
      <div class="col-12">
        <sort-module defaultsort="score">
        </sort-module>
        ${this.allPosts.post.map(i => {
          let postURL = new URL('/topic/post/index.html', window.MyAppGlobals.clientURL);
          postURL.search = `tid=${i.topic}&pid=${i.pid}`;
          
          return html`
          <single-post-module class="row mb-1"
            posturl="${postURL.toString()}"
            title="${i.title}"
            username="${i.username}"
            postdate="${(new Date(i.date)).toDateString()}"
            score="${Number(i.vote_score)}"
            comments="${Number(i.comment_count)}">
          </single-post-module>
          <!-- TODO delete button for admin, owner, mod-->
          `;
        })}
        ${this.allPosts.total > this.allPosts.increment?
          html`
          <pagination-module class="row"
            total="${this.allPosts.total}"
            ref="${this.allPosts.ref}"
            increment="${this.allPosts.increment}">
          </pagination-module>
        `:""
        }
        </div>
      </div>
    `;
  }
}

customElements.define('all-post-list-module', allPostListModule);