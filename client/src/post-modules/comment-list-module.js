import { LitElement, html } from 'lit-element';
import '/src/post-modules/comment-module.js';

export class CommentListModule extends LitElement {

	static get properties() {
		return {
			comment: {type: Array},
		}
	}
  constructor() {
		super();
		this.comment = [
      {
        username: "relevant username",
        profileimg: "/static/images/DefaultUser.png",
        comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \
				Suspendisse auctor euismod cursus. Aliquam vestibulum, nisl sit amet varius \
				finibus, nibh velit ullamcorper erat, tristique molestie dolor risus vel risus. \
				Aliquam in nunc ex. Ut eu molestie dolor, ut iaculis nisl. \
				Donec elementum est vel quam venenatis, quis feugiat ante dignissim. \
				Aliquam luctus ex id magna mattis, ac condimentum lacus tempor. \
				Nunc nibh sem, sagittis sit amet faucibus a, feugiat at risus.",
        date: "date added",
      	score: 0,
      },
      {
        username: "relevant username2",
        profileimg: "/static/images/DefaultUser.png",
        comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \
				Suspendisse auctor euismod cursus. Aliquam vestibulum, nisl sit amet varius \
				finibus, nibh velit ullamcorper erat, tristique molestie dolor risus vel risus. \
				Aliquam in nunc ex. Ut eu molestie dolor, ut iaculis nisl. \
				Donec elementum est vel quam venenatis, quis feugiat ante dignissim. \
				Aliquam luctus ex id magna mattis, ac condimentum lacus tempor. \
				Nunc nibh sem, sagittis sit amet faucibus a, feugiat at risus.",
        date: "date added2",
        score: 1,
      }
    ];
  }

  render() {
    return html`
			
			<ul class="list-unstyled">
      ${this.comment.map(i => {
				return html`
					<comment-module>
              <img slot="profileimg" src = "${i.profileimg}" alt = "profile picture" class="mr-3 img-fluid col-sm-2 rounded-circle">
                <h5 slot = "username" class="mt-0 mb-1">${i.username}</h5>
                <span slot = "comment">${i.comment}</span>
                <span slot = "score"> Score: ${i.score} </span> 
                <span slot = "date"> ${i.date}</span>  
						</comment-module>
        <!-- TODO delete button for admin, owner, mod-->
        `;
      })}
			</ul> 
  `;
  }
}
customElements.define('comment-list-module', CommentListModule);