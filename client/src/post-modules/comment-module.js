import { LitElement, html} from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';
import '../post-modules/update-comment-module.js';

class comment extends LitElement {
  
  static get properties() {
    return {
      uid: {type: Number},
      cid: {type: Number},
      usertype: {type: String},
      username: {type: String},
      profileimg: {type: String},
      comment: {type: String},
      date: {type: String},
      score: {type: Number},
      privilege: {type: Boolean},
      owner: {type: Boolean},
      mod: {type: Boolean},
      profileusertype: {type: String},
      profileusername: {type: String},
      didivote:{type: Number},
      userTypeImg: {type: String},
      userDescription: {type: String},
      banned: {type: Number}
    };

  }
  constructor() {
    super();

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    this.userTypeImg = "";
    this.userDescription = "";
  }
  
  render()    {
    if (this.profileusername === this.username)  {
      this.privilege = true;
      this.owner = true;
    }else if ((this.profileusertype === "moderator" || this.profileusertype === "admin") && this.usertype !== "admin") {
      this.privilege = true;
      this.mod = true;
    }else if (this.usertype === "admin") 
      this.privilege = false;
    else if (this.profileusertype === "user")
      this.privilege = false;

    this.userTypeImg = "/node_modules/bootstrap-icons/bootstrap-icons.svg";
    this.userDescription = "Usertype: ";
    if(this.banned === 0) {
      switch(this.usertype) {
        case "admin":
          this.userTypeImg = this.userTypeImg + "#tools";
          this.userDescription = this.userDescription + "Administrator";
          break;
        case "moderator":
          this.userTypeImg = this.userTypeImg + "#wrench";
          this.userDescription = this.userDescription + "Moderator";
          break;
        case "user":
          this.userTypeImg = this.userTypeImg + "#person-fill";
          this.userDescription = this.userDescription + "User";
          break;
      };
    }else if(this.banned === 1){
      this.userTypeImg = this.userTypeImg + "#person-x-fill";
      this.userDescription = this.userDescription + "Banned";
    } 

    this.profileimg = this.profileimg == "" ? "/static/images/DefaultUser.png" : this.profileimg;
    return html`
      <li class="media">
       <div class="d-flex flex-column">
            <button @click="${this._upvote}" type="button" class="btn bg-transparrant btn-sm">
              <svg class="bi" width="1em" height="1em" fill="currentColor">
               <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#arrow-up" />
              </svg>
            </button>
            <div class="d-flex justify-content-center">
            <small>${(this.score)+(this.didivote)}</small>
            </div>
            <button @click="${this._downvote}" type="button" class="btn bg-transparrant btn-sm">
              <svg class="bi" width="1em" height="1em" fill="currentColor"> 
                <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#arrow-down" />
              </svg>
           </button>
          </div>
          <img src = "${this.profileimg}" alt = "test img"
        class="col-2 rounded-circle">
        <div class = "media-body">
          <div class="d-flex justify-content-between">
          <h5 class="mt-0 mb-1"> ${this.username}
            <svg class="bi" width="1em" height="1em" fill="currentcolor" 
                data-toggle="tooltip" title="${this.userDescription}">
              <use href="${this.userTypeImg}">
            </svg>
          </h5>
           <div>
            ${this.privilege ?
                    html`
                    <div class="dropdown dropleft">
                      <button @click="${this.toggle}" class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg class="bi" width="1em" height="1em" fill="currentcolor">
                          <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#three-dots"/>
                        </svg>
                      </button>
                    <div id = "dropMe" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      ${this.owner ?
                      html`
                        <button class="dropdown-item" 
                        type="button"
                        data-toggle="modal"
                        data-target="updateCommentModal"
                        @click="${this.toggleModal}">Update</button>
                        <button @click="${this._delete}" class="dropdown-item" type="button">Delete</button>` :
                      html``}
                      ${this.mod ?
                      html`
                        <button @click="${this._delete}" class="dropdown-item" type="button">Delete</button>
                        <button @click="${this._block}" class="dropdown-item btn-danger" type="button">Block user</button>
                        ` :
                      html``}
                    </div>
                  </div>
                </div>` :
                    html``}
           </div>
          </div>
          <div class="d-flex w-100 justify-content-between">
          <p> ${this.comment}</p>
          </div>
          <div class = "d-flex w-100 justify-content-between">
            <small>date posted: ${(new Date(this.date)).toDateString()}</small>
           </div>
         </div>
         <update-comment-module class="modal fade" 
                                id="comment_${this.cid}" 
                                tabindex="-1" 
                                role="dialog"
                                cid = "${this.cid}" 
                                comment = "${this.comment}">
         </update-comment-module>
        </div>
      </li>

    `;
  }

  toggleModal(e) {
    let modalElem = this.shadowRoot.querySelector(`#comment_${this.cid}`);
    $(modalElem).modal('toggle');
  }

  toggle(e) {
    let dropMe = this.shadowRoot.querySelector("#dropMe");
    dropMe.classList.toggle("show");
  }

  _upvote(e) {
    const commentVote = serverFetch("commentVote", {cid: this.cid, vote: 1}, true);
    commentVote.then(Response => {
      if (Response.status === 200) {
        this.didivote = 1;
        this.requestUpdate();
      }else {
        console.warn(Response)
      }
    })
    .catch(Response => {
      console.error(Response);
    })
  }

  _downvote(e) {
    const commentVote = serverFetch("commentVote", {cid: this.cid, vote: -1}, true);
    commentVote.then(Response => {
      if (Response.status === 200) {
        this.didivote = -1;
        this.requestUpdate();
      }else {
        console.warn(Response)
      }
    })
    .catch(Response => {
      console.error(Response);
    })
  }

  _delete(e) {
    const deleteComment = serverFetch("deleteComment", {cid: this.cid}, true);
    deleteComment.then(Response => {
      if (Response.status === 200) {
        location.reload();
      }else {
        console.warn(Response);
      }
    })
    .catch(Response => {
      console.error(Response);
    })
  }

  _block(e) {
    console.debug(`Delete user ${this.username}`);
    const banUser = serverFetch("banUser", {uid: this.uid}, true);
    banUser.then(Response => {
      if (Response.status === 200) {
        console.log("user blocked");
        window.alert("User will be unable to login next time");
      }else {
        console.warn(Response)
      }
    })
    .catch(Response => {
      console.error(Response);
    })
  }
}

customElements.define('comment-module', comment);