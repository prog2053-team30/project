import { LitElement, html } from 'lit-element';
import { blockDisplay } from '../moduleStyle.js';
import { serverFormData } from '../scripts/serverFormData.js';
import { charCount } from '../scripts/charCount.js';

export class newPost extends LitElement {
  static getStyles() {
      return blockDisplay;
  }

  static get properties() {
    return {
      pageData: {type: Object},
      tid: {type: String},
      post: {type: Object}
    };
  }
  
  constructor() {
    super();
    
    this.post = {
      minLength: 3,
      helpText: "",
      title: {
        maxLength: 200
      },
      body: {
        maxLength: 20000
      }
    }
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      document.title = "New post" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  firstUpdated() {
    // Extract topic ID from URL
    this.tid = (new URL(window.location).searchParams).get('tid');
    const newPostForm = this.shadowRoot.querySelector('#new-post-form');
    
    this.post.helpText = `Please write at least ${this.post.minLength} characters.`;
    
    // Capture submit event
    newPostForm.addEventListener('submit', (e) => {
      // Disable default submit action
      e.preventDefault();
      
      // Use custom action
      this._createPost(newPostForm);
    });
  }

  render() {
    return html`
    <form id="new-post-form">
      <h2> Create post </h2>
      <div class="form-group">
        <label for="title"> Title </label>
        <!-- TODO:add rows and cols -->
        <input type="text"
               id="title"
               name="title"
               class="form-control"
               minlength="${this.post.minLength}"
               maxlength="${this.post.title.maxLength}"
               @keyup="${this._elemGrab}"
               required>
        <small>${this.post.helpText}</small>
      </div>
      <div class="form-group">
        <label for="content"> Post content </label>
        <!-- TODO:add rows and cols -->
        <textarea id="content" 
                  class="form-control"
                  name="content"
                  minlength="${this.post.minLength}"
                  maxlength="${this.post.body.maxLength}"
                  @keyup="${this._elemGrab}"
                  required></textarea>
        <small>${this.post.helpText}</small>
      </div>
      <button type="submit" 
              class="btn btn-primary">
        Create post
      </button>
    </form>
    `;
  }
  
  /**
   * Creates a post with data from 'form'
   * 
   * @param {*} form  - Form with data to be sent to server
   */
  _createPost(form) {
    const data = new FormData(form);
    
    // Where the new post will be found after creation
    let newPostURL = new URL("topic/post/index.html", window.MyAppGlobals.clientURL);
    
    // Add topic ID to FormData object
    data.set('tid', this.tid);
    
    serverFormData("newPost", data).then( Response => {
      if (Response.pid > 0) {
        newPostURL.searchParams.append('tid', this.tid);
        newPostURL.searchParams.append('pid', Response.pid);
        // Redirect to the new post
        window.open(newPostURL, "_self");
      } else {
        //TODO: Display error message to user
        console.error("Something went wrong creating the new post");
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }
  
  /**
   * @brief Grabs the target ov the event and the next sibling which is sent to charCount
   * 
   * @param {*} e   - The event that called the function
   * @see charCount(...)
   */
  _elemGrab(e) {
    let inputNode = e.target;
    let outputNode = inputNode.nextElementSibling;
    
    // Count characters from input and write to output
    charCount(inputNode, outputNode);
  }
}

customElements.define('new-post-module', newPost);