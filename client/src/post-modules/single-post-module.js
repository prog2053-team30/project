import { html, LitElement } from 'lit-element';
import { singlePostStyle, partStyle } from '../moduleStyle';

class singlePost extends LitElement {
  static get styles() {
    return [singlePostStyle, partStyle];
  }
  
  static get properties() {
    return {
      postURL: { type: String },
      title: { type: String },
      username: { type: String },
      postDate: { type: String },
      score: { type: Number },
      comments: { type: Number }
    };
  }
  
  constructor() {
    super();
  }

  render() {
    return html`
    <div class="col-12 mb-1 part-style">
      <div class="postDetails flex-fill position-relative">
      <h5 class="d-block text-truncate"> 
        <a href="${this.postURL}" class="text-dark">${this.title}</a>
      </h5>
      <div class="d-flex">
        <div class="d-flex m-0 p-0 col-8">
          <p class="d-inline-block m-0 p-0 col-5 text-left text-truncate">
            By: ${this.username}
          </p>
          <p class="d-inline-block m-0 p-0 col-4 text-center text-truncate">
            Score: ${this.score}
          </p>
          <p class="d-inline-block m-0 p-0 col-3 text-center text-truncate">
            Comments: ${this.comments}
          </p>
        </div>
        <p class="d-inline-block m-0 col-4 text-right text-truncate">
          Posted: ${this.postDate}
        </p>
      </div>
    </div>
  </div>
    `;
  }
}

window.customElements.define('single-post-module', singlePost);