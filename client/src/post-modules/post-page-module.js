import { html, LitElement } from 'lit-element';
import { blockDisplay } from '../moduleStyle.js'; 
import { serverFetch } from '../scripts/serverFetch.js';
import { serverFormData } from '../scripts/serverFormData.js';
import { charCount } from '../scripts/charCount.js';
import '../generic-modules/pagination-module.js';
import '../generic-modules/sort-module.js';
import '../post-modules/comment-list-module.js';
import '../post-modules/update-module.js';

export class postPage extends LitElement {
  static get Styles() {
    return blockDisplay;
  }
  
  static get properties() {
    return {
      pageData: {type: Object},
      topic: {type: Object},
      post: {type: Object},
      comment: {type: Object},
      comments: {type: Object},
      profile: {type: Object},
      privilege: {type: Boolean},
      owner: {type: Boolean},
      mod: {type: Boolean},
      userDescription: {type: String},
      userTypeImg: {type: String},
      helpText: {type: String},
      newComment: {type: Object}
    };
  }
  
  constructor() {
    super();

    this.userTypeImg = "";
    this.userDescription = "";
    this.helpText = "";
    this.newComment ={
      minLength: 3,
      helpText: "",
      maxLength: 20000
    }
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    let params = (new URL(window.location)).searchParams;

    let sortBy;
    
    this.post = {
      id: params.get('pid'),
      uid: 0,
      didIVote: 0,
      username: "",
      title: "",
      date: "",
      dateString: "",
      score: 0,
      content: "",
      picture: "",
      userType: ""
    };
    
    this.topic = {
      id: params.get('tid'),
      name: "Relevant topic",
      url: new URL("/topic/index.html", `${window.MyAppGlobals.clientURL}`)
    };

    this.comments = {
      total: 0,
      ref: 0,
      increment: 25,
      sort: "score",
      comment: []
    };

    this.profile = {
      username: "something random",
      userType: "something radnom",
      uid: 1
    };

    if (params.has('ref')) {
      this.comments.ref = params.get('ref');
    }

    // If a sort method is stored in the URL, use that.
    if (params.has('sort')) {
      sortBy = params.get('sort');
    } else {
      sortBy = this.comments.sort;
    }
    
    // Append the topic id to the topic URL
    this.topic.url.search = `tid=${this.topic.id}`;
    
    //TODO: Fetch post data from server.
    const postData = serverFetch("post", {
      pid: Number(this.post.id), 
      ref: this.comments.ref,
      sort: sortBy,
      increment: this.comments.increment
    });
    
    postData.then( Response => {
      this.post.title = Response.title;
      this.post.uid = Response.user_id;
      this.post.username = Response.username;
      this.post.date = new Date(Response.date);
      this.post.dateString = new Date(Response.date).toDateString();
      this.post.score = Number(Response.score);
      this.post.content = Response.content;
      this.post.picture = Response.userPicture;
      this.post.didIVote = Response.didIVote;
      this.post.userType = Response.userType;
      this.post.banned = Response.banned;
      this.comments.total = Response.comment_count;
      this.comments.comment = Response.comments;
      this.post.score -= this.post.didIVote
      this.requestUpdate();
    })
    .catch( Response => {
      console.error(Response);
    });

    

    //TODO determine boolean values based on user ID and comment ID
    const profile = serverFetch("profile");
    profile.then(Response => {
      this.profile.userType = Response.userType;
      this.profile.username = Response.username;
      this.profile.uid = Response.uid; 
      this.requestUpdate();
    })
    .catch(Response => {
      console.error(Response);
    })

    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
    
      document.title = "Post" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }

    firstUpdated() {
      // Extract post ID from URL
      this.pid = (new URL(window.location).searchParams).get('pid');
      const newCommentForm = this.shadowRoot.querySelector('#new-comment-form');

      this.newComment.helpText = `Please type at least ${this.newComment.minLength} characters.`;

    newCommentForm.addEventListener('submit', (e) => {
      // Disable default submit action
      e.preventDefault();
      
      // Use custom action
      this._createComment(newCommentForm);
    });
  }
  
  render() {
    if (this.profile.username === this.post.username)  {
      this.privilege = true;
      this.owner = true;
      console.log("owner");
    }else if ((this.profile.userType === "moderator" || this.profile.userType === "admin") && this.post.userType !== "admin") {
      this.privilege = true;
      this.mod = true;
      console.log("mod/admin")
    }else if (this.post.userType === "admin") {
      this.privilege = false;
      console.log("adminpost");
    }else if (this.profile.userType === "user") {
      this.privilege = false; 
      console.log("user");
    }

    this.userTypeImg = "/node_modules/bootstrap-icons/bootstrap-icons.svg";
    this.userDescription = "Usertype: ";
    if(this.post.banned === 0) {
      switch(this.post.userType) {
        case "admin":
          this.userTypeImg = this.userTypeImg + "#tools";
          this.userDescription = this.userDescription + "Administrator";
          break;
        case "moderator":
          this.userTypeImg = this.userTypeImg + "#wrench";
          this.userDescription = this.userDescription + "Moderator";
          break;
        case "user":
          this.userTypeImg = this.userTypeImg + "#person-fill";
          this.userDescription = this.userDescription + "User";
          break;
      };
    }else if(this.post.banned === 1){
      this.userTypeImg = this.userTypeImg + "#person-x-fill";
      this.userDescription = this.userDescription + "Banned";
    }

    this.post.picture = this.post.picture == "" ? "/static/images/DefaultUser.png" : this.post.picture;  
    return html`
      <a href="${this.topic.url.toString()}" class="btn btn-secondary my-2">${this.topic.name}
        <svg class="bi" width="1em" height="1em" fill="currentColor">
          <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#arrow-return-left" />
        </svg>
      </a>
      <div class="d-flex justify-content-start">
        <div class="d-flex flex-column">
          <button type="button" class="btn bg-transparrant btn-sm" @click="${this._upvote}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
             <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#arrow-up" />
            </svg>
          </button>
          <div class="d-flex justify-content-center">
            <small>${this.post.score+this.post.didIVote}</small>
          </div>
          <button type="button" class="btn bg-transparrant btn-sm" @click="${this._downvote}">
            <svg class="bi" width="1em" height="1em" fill="currentColor"> 
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#arrow-down" />
            </svg>
          </button>
        </div>
        <div class="col-2">
          <img src = "${this.post.picture}" alt = "OP profile image"
          class="img-fluid rounded-circle">
        </div>
        <div class="w-100">
          <div class="d-flex w-100 justify-content-between">
            <h2 class="mb-0"> ${this.post.title}</h2> 
              <div>
                ${this.privilege ?
                  html`
                  <div class="dropdown dropleft">
                    <button @click="${this.toggle}" class="btn" type="button" 
                      id="dropdownMenuButton" data-toggle="dropdown">
                      <svg class="bi" width="1em" height="1em" fill="currentcolor">
                        <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#three-dots"/>
                      </svg>
                    </button>
                    <div id = "dropMe" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      ${this.owner ?
                      html`
                        <button class="dropdown-item" 
                        type="button"
                        data-toggle="modal"
                        data-target="updateModal"
                        @click="${this.toggleModal}">Update</button>
                        <button @click="${this._delete}" class="dropdown-item" type="button">Delete</button>` :
                      html``}
                      ${this.mod ?
                      html`
                        <button @click="${this._delete}" class="dropdown-item" type="button">Delete</button>
                        <button @click="${this._block}" class="dropdown-item" type="button">Block user</button>
                        ` :
                      html``}
                    </div>
                  </div>
                </div>` :
              html``}
          </div>
        </div>   
        <small class="d-block mb-2"> Submitted ${this.post.dateString} by: ${this.post.username}
          <svg class="bi" width="1em" height="1em" fill="currentcolor" 
                data-toggle="tooltip" title="${this.userDescription}"
                data-original-title="${this.userDescription}">
            <use href="${this.userTypeImg}">
          </svg>
        </small>
        <p> ${this.post.content}</p>
      </div>
      <update-module class="modal fade" 
                     id="updateModal" 
                     tabindex="-1" 
                     role="dialog"
                     pid = "${Number(this.post.id)}" 
                     title = "${this.post.title}"
                     content = "${this.post.content}">
      </update-module>
    </div>
        
      <!-- TODO remove button for admin, owner, mod-->
    <h3>Comments:</h3>
    <form id="new-comment-form">
      <div class="form-group">
        <label for="comment"> Post comment </label>
        <!-- TODO:add rows and cols -->
        <textarea id="comment" 
                  class="form-control" 
                  name="comment"
                  minlength="${this.newComment.minLength}" 
                  maxlength="${this.newComment.maxLength}"
                  @keyup="${this._elemGrab}"
                  required></textarea>
        <small>${this.newComment.helpText}</small>
      </div>
      <div class="form-group">
        <button type="submit" 
          class="btn btn-primary">
            Post comment
        </button>
      </div>
    </form>
    <sort-module defaultsort="${this.comments.sort}">
    </sort-module>
    <ul class="list-unstyled">
      ${this.comments.comment.map(i => {
       return html`
          <comment-module class="mb-2 d-block"
            profileusertype ="${this.profile.userType}"
            profileusername ="${this.profile.username}"
            banned="${i.banned}"
            uid="${i.user}"
            cid="${i.cid}"
            didivote = "${i.DidIVote}"
            profileimg = "${i.picture}"
            username = "${i.username}"
            usertype = "${i.userType}"
            comment = "${i.comment}"
            score = "${i.comment_score - i.DidIVote}"
            date = "${i.date}"
            ?privilege = "${i.privilege}"
            ?owner = "${i.owner}"
            ?mod = "${i.mod}"> 
          </comment-module>
        <!-- TODO delete button for admin, owner, mod-->
        `;
      })}
      ${(this.comments.total > this.comments.increment)?
        html`
        <pagination-module class="row"
          total="${this.comments.total}"
          ref="${this.comments.ref}"
          increment="${this.comments.increment}">
          </pagination-module>
        `:""
      }
      </ul> 
    `;
 
  }

  _createComment(form) {
    const data = new FormData(form);
    
    // Add topic ID to FormData object
    data.set('pid', this.pid);
    
    serverFormData("newComment", data, true).then( Response => {
      if (Response.status===200) {
        window.location.reload();
      } else {
        //TODO: Display error message to user
        console.error("Something went wrong creating the new comment");
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }
  
  _elemGrab(e) {
    let inputNode = e.target;
    let outputNode = inputNode.nextElementSibling;
    
    charCount(inputNode, outputNode);
  }

  toggle(e) {
    let dropMe = this.shadowRoot.querySelector("#dropMe");
    dropMe.classList.toggle("show");
  }

  toggleModal(e) {
    $('update-module').modal('toggle');
  }

  _upvote(e) {
    console.debug(`vote + 1 for post ${this.post.title} from user ${this.profile.username}`);
    const postVote = serverFetch("postVote", {pid: this.post.id, vote: 1}, true);
    postVote.then(Response => {
      if (Response.status === 200) {
          this.post.didIVote = 1;
          this.requestUpdate();
        }else {
        console.warn(Response)
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }

  _downvote(e) {
    console.debug(`vote + 1 for post ${this.post.title} from user ${this.profile.username}`);
    const postVote = serverFetch("postVote", {pid: this.post.id, vote: -1}, true);
    postVote.then(Response => {
      if (Response.status === 200) {
        this.post.didIVote = -1;
        this.requestUpdate();
      }else {
        console.warn(Response)
      }
    })
    .catch( Response => {
      console.error(Response);
    })
  }

  _delete(e) {
    const redirectTopic = new URL (window.location.href);
    redirectTopic.pathname = "/topic/index.html";
    redirectTopic.searchParams.delete("pid")
    console.debug(`Delete post with id ${this.post.id}`);
    const deletePost = serverFetch("deletePost", {pid: this.post.id}, true);
    deletePost.then(Response => {
      if (Response.status === 200) {
        window.location.replace(redirectTopic);
      }else {
        console.warn(Response)
      }
    })
    .catch(Response => {
      console.error(Response);
    })
  }

  _block(e) {
    console.debug(`Delete user ${this.post.username}`);
    const banUser = serverFetch("banUser", {uid: this.post.uid}, true);
    banUser.then(Response => {
      if (Response.status === 200) {
        console.log("user blocked");
        window.alert("User will be unable to login next time");
      }else {
        console.warn(Response)
      }
    })
    .catch(Response => {
      console.error(Response);
    })
  }
}

customElements.define('post-page-module', postPage);