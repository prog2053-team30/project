import { LitElement, html } from 'lit-element';
import '../post-modules/single-post-module.js';

export class PostListModule extends LitElement {

    static get properties() {
        return {
        tID: {type: Number},
        post: {type: Array}
        }
    }
    
    constructor() {
        super();
        this.tID = 1;
        this.post = [
            {
              title: "Relevant post title",
              date: "date added",
              score: 0,
              comments: 0,
              username: "relevant username",
            },
            {
              title: "Relevant post title2",
              date: "date added2",
              score: 1,
              comments: 1,
              username: "relevant username2",
            }
          ];//TODO fetch contents
    }

    render() {
        return html`
        <h3>Posts:</h3>
        ${this.post.map(i => {
          return html`
        <single-post-module>
          <h5 slot = "title" class="mt-0">
            <a href="/topic/post/index.html" class="text-dark">
              ${i.title}
            </a>
          </h5>
          <span slot = "date"> ${i.date}</span>  
          <span slot = "score"> Score: ${i.score} </span> 
          <span slot = "comments"> Comments: ${i.comments} </span>
          <span slot = "username"> Posted by: ${i.username} </span>
        </single-post-module>
        <!-- TODO delete button for admin, owner, mod-->
          `;
        })}
        `;
    }
}


window.customElements.define('post-list-module', PostListModule);