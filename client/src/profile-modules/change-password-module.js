import { LitElement, html } from 'lit-element';
import { profileStyle } from '../moduleStyle';
import { serverFormData } from '../scripts/serverFormData';

class ChangePasswordModule extends LitElement {
  static get styles() {
    return profileStyle;
  }
  
  constructor() {
    super();
  }
  
  firstUpdated() {
    let formElem = this.shadowRoot.querySelector('form');
    
    formElem.addEventListener('submit', (e) => {
      e.preventDefault();
      
      this._changePassword(formElem);
    });
  }
  
  render() {
    let passwordPattern = `[${window.MyAppGlobals.password.validCharacters}]` +
                              `{${window.MyAppGlobals.password.minlength},` +
                               `${window.MyAppGlobals.password.maxlength}` +
                              `}`;
    return html`
    <div>
      <h3>Change password</h3>
      <form>
        <div class="form-group">
          <label for="current-password">Current password</label>
          <input type="password" 
                 name="currentPassword" 
                 id="current-password" 
                 minlength="${window.MyAppGlobals.password.minlength}"
                 maxlength="${window.MyAppGlobals.password.maxlength}"
                 pattern="${passwordPattern}"
                 class="form-control"
                 required>
        </div>
        <div class="form-group">
          <label for="new-password">New password</label>
          <input type="password" 
                 name="newPassword" 
                 id="new-password"
                 minlength="${window.MyAppGlobals.password.minlength}"
                 maxlength="${window.MyAppGlobals.password.maxlength}"
                 pattern="${passwordPattern}"
                 class="form-control"
                 required>
          <small class="form-text text-muted">
            Your password must be 8-50 characters long, and can contain letters, 
            numbers, spaces and these symbols !"#$%&'()*+´-./:;<=>?@[\\]^_{|}~
          </small>
        </div>
        <div class="form-group">
          <label for="confirm-new-password">Confirm new password</label>
          <input type="password" 
                 name="confirmNewPassword" 
                 id="confirm-new-password"
                 minlength="${window.MyAppGlobals.password.minlength}"
                 maxlength="${window.MyAppGlobals.password.maxlength}"
                 pattern="${passwordPattern}"
                 class="form-control"
                 required>
        </div>
        <button type="reset" class="btn btn-secondary">Reset form</button>
        <button type="submit" class="btn btn-primary">
          Change password
        </button>
      </form>
    </div>
    `;
  }
  
  _changePassword(form) {
    const data = new FormData(form);
    serverFormData("changePassword", data, true).then( Response => {
      if (Response.status === 200) {
        location.reload();
      } else {
        console.warn(Response);
      }
    })
    .catch( Response => {
      console.error(Response);
    });
  }
};

window.customElements.define("change-password-module", ChangePasswordModule);