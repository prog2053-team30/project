import { LitElement, html } from 'lit-element';
import { uniqueUsername } from '../scripts/uniqueUsername.js';
import { serverFormData } from '../scripts/serverFormData.js';
import { profileStyle } from '../moduleStyle.js';

class UserInfoModule extends LitElement {
  static get styles() {
    return profileStyle;
  }
  
  static get properties() {
    return {
      currentUsername: {type: String},
      email: {type: String},
    }
  }
  
  constructor() {
    super();
  }
  
  firstUpdated() {
    let formElem = this.shadowRoot.querySelector('form');
    
    formElem.addEventListener('submit', (e) => {
      e.preventDefault();
      
      this._saveUserinfo(formElem);
    });
  }
  
  render() {
    if (this.email === "") {
      this.email = "(Optional)";
    }
    return html`
    <div>
      <h2 class="my-0">User information</h2>
      <form>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" 
                 name="username" 
                 id="username" 
                 class="form-control"
                 placeholder="${this.currentUsername}"
                 minlength="3"
                 maxlength="40"
                 pattern="[a-zA-Z0-9]{3,40}">
          <small class="form-text text-muted">
            Your username must be <span id="usrCharLim">3-40 characters</span> long, and 
            <span id="usrCharVal">only contain letters (a-z or A-Z) and numbers</span>.
          </small>
        </div>
        <div class="form-group">
          <label for="email">E-mail adress</label>
          <input type="email" 
                 name="email" 
                 id="email" 
                 class="form-control"
                 pattern="[a-zA-Z0-9.!#$%&’*+/=?^_\`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*"
                 placeholder="${this.email}">
        </div>
        <button type="reset" class="btn btn-secondary">Reset form</button>
        <button type="submit" class="btn btn-primary">
          Save changes
        </button>
      </form>
    </div>
    `;
  }
  
  async _saveUserinfo(form) {
    const data = new FormData(form);
    
    let formContent = {};
    let username = {
      change: false,      // Has the user actually changed their username
      unique: false,      // Is the username unique in our db
      valid() {
        return (username.unique && username.change || !username.change);
      }
    }
    
    let email = {
      change: false,
      valid() {
        return (email.unique && mail.change || !email.change);
      }
    }
    
    // Extract data pairs from the FormData object.
    for (let pair of data.entries()) {
      formContent[pair[0]] = pair[1].toString();
    }

    // Check if new username is different from the current.
    username.change = ((formContent.username != this.currentUsername) && 
                        formContent.username.length > 0 );
    
    // If the username is changed
    if (username.change) {
      // Ask db if the username exists
      username.unique = await uniqueUsername(formContent.username);
      
      if (!username.unique) {
        //TODO: Display message to user.
        console.debug("The new username is already taken.");
      }
    } else {
      console.debug("Username is not changed");
    }
    
    // check whether email is actually changed if it already had one
    email.change = (formContent.email != email.current) && formContent.email.length > 0;
    if (!email.change)  {
      console.debug("Email did not change");
    }
   
    if (!(email.valid() || username.valid()) || (username.change || email.change))  {
      serverFormData('changeProfileInfo', data, true).then( Response => {
        if (Response.status === 200) {
          console.log(200)
        } else if (Response.status === 401) {
          console.log(401)
        } else {
          console.error("Unexpected response: " + Response.status);
        }
      }).catch( Response => {
        console.error(Response)
      })
    }
  }
}

window.customElements.define("user-info-module", UserInfoModule);