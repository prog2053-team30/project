import { LitElement, html } from 'lit-element';
import { profileStyle } from '../moduleStyle.js';
import { serverFetch } from '../scripts/serverFetch.js';

export class UserStatusModule extends LitElement {
  static get styles() {
    return profileStyle;
  }
  
  static get properties() {
    return {
      usertype: {type: String},
      modRequest: {type: String}
    }
  }
  
  constructor() {
    super();
  }
  
  render() {
    return html`
    <div>
      <h3>User status</h3>
      <span>Usertype: ${this.usertype}</span>
      ${this.usertype === "user"?
        this.typeUser():
        ""}
      ${this.usertype === "moderator"?
        this.typeMod():
        ""}
      ${this.usertype === "admin"?
        this.typeAdmin():
        ""}
    </div>
    `;
  }
  
  typeUser() {
    return html`
      ${this.modRequest == "no"?
        html`<button class="btn btn-warning" @click="${this._reqMod}">Request moderator status</button>`:
        html`<p>Moderator status request: ${this.modRequest}</p>`
      }
      `;
  }
  
  typeMod() {
    return html`
      <a href="/site/mod.html" class="btn btn-primary">Moderator page</a>
      `;
  }
  
  typeAdmin() {
    return html`
      <a href="/site/admin.html" class="btn btn-primary">Administrator page</a>
      `;
  }
  
  _reqMod(e) {
    serverFetch("requestMod", {}, true).then( Response => {
      if (Response.status === 200) {
        console.debug(e.target);
      } else {
        console.warn("Something went wrong requesting moderator status.");
      }
      window.location.reload();
    })
    .catch( Response => {
      console.error(Response);
    });
  }
}

customElements.define('user-status-module', UserStatusModule);