import { LitElement, html } from 'lit-element';
import { profileStyle } from '../moduleStyle';
import {serverFormData} from "../scripts/serverFormData";

class ProfilePictureModule extends LitElement {
  static get styles() {
    return profileStyle;
  }

  static get properties() {
    return {
      profilePicture: {type: String},
      defaultImg: {type: String},
    }
  }

  constructor() {
    super();
    // The default profile picture, is changed if the user has added a picture
    this.defaultImg = "/static/images/DefaultUser.png";
  }
  
  render() {
    if (this.profilePicture === "" || this.profilePicture === undefined || this.profilePicture === "undefined") {
      this.profilePicture = this.defaultImg;
    }
    return html`
    <div>
      <h3>Profile picture</h3>
      <img src="${this.profilePicture}"
            alt="Profile picture"
            class="rounded-circle img-fluid img-thumbnail mb-2">
      <form class="m-1 w-auto">
        <div class="input-group">
          <div class="custom-file">
            <input type="file" id="profile-picture" class="custom-file-input" name="ProfilePictureUpload">
            <label class="custom-file-label" for="profile-picture">
              Select picture
            </label>
          </div>
          <div class="input-group-append">
            <button type="button" @click="${this._upload}" class="btn btn-primary">
              Save
            </button>
          </div>
        </div>
      </form>
    </div>
    `;
  }

  _upload(e) {
    const data = new FormData(e.target.form);
    serverFormData('profilePictureUpload', data, true).then( Response => {
      if (Response.status === 200) {
        console.log(200)
      } else if (Response.status === 401) {
        console.warn(Response)
      } else {
        console.error("Unexpected response: " + Response.status);
      }
    }).catch( Response => {
      console.error(Response)
    })
  }
}

window.customElements.define("profile-picture-module", ProfilePictureModule);