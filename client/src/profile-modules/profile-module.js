import { LitElement, html } from 'lit-element';
import { serverFetch } from '../scripts/serverFetch.js';
import './profile-picture-module.js';
import './user-info-module.js';
import './user-status-module.js';
import './change-password-module.js';

export class UserProfile extends LitElement{
  static get properties() {
    return {
      pageData: {type: Object},
      user: {type: Object}
    }
  }
  
  constructor() {
    super();
    const fetchInfo = serverFetch("profile");
    
    this.pageData = {};
    
    this.user = {};
    fetchInfo.then( res => {
      this.user = res;
    })
    .catch( res => {
      console.error(res);
    })
    
    window.MyAppGlobals.staticInfoPromise.then( () => {
      this.pageData = window.MyAppGlobals.staticInfoData;
      document.title = "Profile" + this.pageData.site.separator + this.pageData.site.name;
    })
    .catch( result => {
      console.error(result);
    });
  }
  
  render() {
    return html`
    <div class="row">
      <profile-picture-module class="col-sm-5 col-xs-12 p-2"
        profilepicture="${this.user.picture}"></profile-picture-module>
      <user-info-module class="col-sm-7 col-xs-12 p-2"
        currentusername="${this.user.username}"
        email="${this.user.email}"></user-info-module>
      <user-status-module class="col-12 my-1 p-2"
        usertype="${this.user.userType}"
        modrequest="${this.user.modRequest}"></user-status-module>
    </div>
    <div class="row">
      <change-password-module class="col-12 my-1 p-2">
      </change-password-module>
    </div>
    `;
  }
}

window.customElements.define("profile-module", UserProfile);