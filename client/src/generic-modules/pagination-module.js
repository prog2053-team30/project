import { LitElement, html } from 'lit-element';
import { paginationStyle } from '../moduleStyle.js';

export class PaginationModule extends LitElement {
  static get styles() {
    return paginationStyle;
  }
  
  static get properties() {
    return {
      total: {type: Number},
      ref: {type: Number},
      refPrefix: {type: String},
      refName: {type: String},
      increment: {type: Number}
    }
  }
  
  constructor() {
    super();
    //TODO: Throw error if any of the properties are undefined here.
  }
  
  // Executes on first 'update'
  firstUpdated() {
    this.pageURL = new URL(window.location);
    this.refName = "ref";
    
    let onFirstPage = this.ref === 0;
    let onLastPage = (this.ref + this.increment) > this.total;
    
    if(this.refPrefix !== undefined) {
      this.refName = this.refPrefix + "-" + this.refName;
    }
    
    if (onFirstPage) {
      this.shadowRoot.querySelectorAll(".backward").forEach( element => {
        element.disabled = true;
      })
    } else if (onLastPage) {
      this.shadowRoot.querySelectorAll(".forward").forEach( element => {
        element.disabled = true;
      })
    }
  }
  
  render() {
    this.numPages = Math.ceil(this.total / this.increment);
    this.pages = [];
    for (let i = 0; i < this.numPages;) {
      this.pages.push(++i);
    }
    return html`
    <nav>
      <ul class="pagination my-2 justify-content-center">
        <li class="page-item">
          <button class="page-link backward" @click="${this._first}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#chevron-double-left" />
            </svg>
          </button>
        </li>
        <li class="page-item">
          <button class="page-link backward" @click="${this._previous}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#chevron-left" />
            </svg>
          </button>
        </li>
        ${this.pages.map( i => {
          let classList = "page-item"
          if ( Math.ceil((this.ref+1)/this.increment) === i ) {
            classList += " active"
          } 
          return html`
            <li class="${classList}">
              <button class="page-link" @click="${this._toPage}">${i}</button>
            </li>
          `
        })}
        <li class="page-item">
          <button class="page-link forward" @click="${this._next}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#chevron-right" />
            </svg>
          </button>
        </li>
        <li class="page-item">
          <button class="page-link forward" @click="${this._last}">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#chevron-double-right" />
            </svg>
          </button>
        </li>
      </ul>
    </nav>
    `;
  }
  
  /**
   * @brief Opens the webpage at ref 0
   * @param {} e - The event itself is not used
   */
  _first(e) {
    this.pageURL.searchParams.set(`${this.refName}`, 0);
    window.open(this.pageURL, "_self");
  }
  
  /**
   * @brief Decrements the ref value with the value of increment and opens the webpage there.
   * @param {*} e - The event itself is not used
   */
  _previous(e) {
    const pageTarget = this.ref - this.increment;
    this.pageURL.searchParams.set(`${this.refName}`, pageTarget);
    window.open(this.pageURL, "_self");
  }
  
  /**
   * @brief Goes to a specific page based on the value in 'e'
   * @param {*} e - The event that called the function, is used to calculate ref from page nr.
   */
  _toPage(e) {
    const pageTarget = (Number(e.target.textContent)-1) * this.increment;
    if( !(e.target.parentElement.classList.contains('active')) ) {
      this.pageURL.searchParams.set(`${this.refName}`, pageTarget);
      window.open(this.pageURL, "_self");
    }
  }
  
  /**
   * @brief Adds the increment value to the ref value and opens the webpage there
   * @param {*} e - The event itself is not used
   */
  _next(e) {
    const pageTarget = this.ref + this.increment;
    this.pageURL.searchParams.set(`${this.refName}`, pageTarget);
    window.open(this.pageURL, "_self");
  }
  
  /**
   * @brief Calculates the last page's ref value and opens the webpage there
   * @param {*} e - The event itself is not used
   */
  _last(e) {
    const pageTarget = (this.numPages - 1) * this.increment;
    this.pageURL.searchParams.set(`${this.refName}`, pageTarget);
    window.open(this.pageURL, "_self");
  }
}

customElements.define('pagination-module', PaginationModule);