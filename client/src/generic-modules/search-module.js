import { LitElement, html } from 'lit-element';

export class SearchModule extends LitElement {
  static get properties() {
    return {
      query: {type: String},
    }
  }
  
  constructor() {
    super();
  }
  
  firstUpdated() {
    let params = (new URL(document.location)).searchParams;
    
    this.query = params.get('q');
  }
  
  render() {
    return html`
    <form class="form-inline" action="/search.html" method="GET">
      <div class="input-group">
        <input type="search" 
               name="q"
               class="form-control"
               placeholder="Search..."
               minlength="3"
               maxlength="500"
               required
               value="${
                 this.query != null?  // If there is a q parameter in the url
                  this.query:   // Show the previous search
                  ""            // If there is no q parameter, leave empty.
               }">
        <div class="input-group-append">
          <button class="btn btn-secondary" type="submit">
            <svg class="bi" width="1em" height="1em" fill="currentColor">
              <use href="/node_modules/bootstrap-icons/bootstrap-icons.svg#search" />
            </svg>
          </button>
        </div>
      </div>
    </form>
    `;
  }
}

window.customElements.define('search-module', SearchModule);