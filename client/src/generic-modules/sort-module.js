import { LitElement, html } from 'lit-element';
import { contentDisplay } from '../moduleStyle.js';

export class SortModule extends LitElement {
  static get styles() {
    return contentDisplay;
  }
  
  static get properties() {
    return {
      defaultSort: {type: String},
      currentSort: {type: String},
      sort: {type: Array}
    }
  }
  
  constructor() {
    super();
    
    this.sort = [
      {
        value: "date",
        name: "Recent"
      },
      {
        value: "score",
        name: "Top"
      }
    ]
  }
  
  firstUpdated() {
    let sortForm = this.shadowRoot.querySelector("#post-sort-form");
    let selectedOption;
    let params = (new URL(window.location)).searchParams;
    
    this.currentSort = this.defaultSort;
    
    // Use the URLs sort parameter if it exists.
    if (params.has('sort')) {
      this.currentSort = params.get('sort');
    }
    
    // Find the currently used value
    selectedOption = sortForm.querySelector(`#${this.currentSort}`);
    
    // Change event of the submit button
    sortForm.addEventListener('submit', (e) => {
      // Disable default action
      e.preventDefault();
      
      // Call custom action
      this._sort(sortForm.querySelector("select"));
    });
    
    // Set the currently used value to selected.
    selectedOption.selected = true;
  }
  
  render() {
    return html`
    <form id="post-sort-form" class="input-group mb-1">
      <div class="input-group-prepend">
        <label class="input-group-text" for="sort-by">Sort posts by:</label>
      </div>
      <select name="sort" id="sort-by" class="custom-select">
        ${this.sort.map( i => {
          return html`
            <option value="${i.value}" id="${i.value}">${i.name}</option>
          `
        })
        }
      </select>
      <div class="input-group-append">
        <button type="submit" class="btn btn-primary">Sort</button>
      </div>
    </form>
    `;
  }
  
  /**
   * @brief Sends the user to a page with the new sorting, if changed.
   * @param {*} select The select element
   */
  _sort(select) {
    if (select.value != this.currentSort) {
      let newURL = new URL(window.location);
      newURL.searchParams.set('sort', select.value);
      window.open(newURL, "_self");
    } 
  }
}

customElements.define('sort-module', SortModule);