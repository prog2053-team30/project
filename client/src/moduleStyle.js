import { css } from 'lit-element';

export const blockDisplay = css`
    :host {
        display: block;
    }
`;

export const contentDisplay = css`
    :host {
        display: contents;
    }
`;

export const flexDisplay = css`
    :host {
        display: flex;
    }
`;

export const headerStyle = css`
    :host {
        background-color: #00509e;
        color: #fff;
    }
`;

export const footerStyle = css`
    :host {
        margin-top: auto;
        background-color: #00509e;
        color: #fff;
    }
`;

export const wrapperStyle = css`
    :host {
        display: flex;
        flex-direction: column;
        min-height: 100vh;
    }
    
    main>div {
        background-color: #e0e0e0;
    }
`;

export const navStyle = css`
    :host {
        background-color: #00509e;
        color: #ffffff;
    }
    
    .nav-link {
        color: #ffffff; 
        background-color: #00509e;
        transition: none;
        border-radius: 0;
    }
    
    .nav-link:hover {
        filter: brightness(0.95);
        color: #ffffff;
    }
    
    .navbar {
        padding: 0;
    }
`;

export const paginationStyle = css`
    :host {
        justify-content: center;
    }
    
    .page-link {
        border-color: #3e628a;
        background-color: #3e628a;
        color: #fff;
        transition: none;
    }
    
    .active>button:hover {
        filter: brightness(1);
        cursor: default;
    }
    
    .page-link:hover {
        background-color: #3e628a;
        border-color: #3e628a;
        filter: brightness(1.1);
        color: #fff;
    }
    
    .page-link:disabled {
        cursor: not-allowed;
    }
    
    .page-link:disabled:hover {
        filter: brightness(1);
    }
`;

export const columnStyle = css`
    @media (min-width: 576px) {
        .card-columns {
            column-count: 2;
        }
    }
`;

export const singleTopicListStyle = css`
    .topicNr {
        padding: 1em;
        min-width: 3em;
    }
    
    .topicDetails {
        padding: .25em;
    }
`;

export const singlePostStyle = css`
    .postDetails {
        padding: .25em;
    }
`;

export const profileStyle = css`
    :host > * {
        background-color: #c4c4c4;
        padding: .5em;
    }
`;

export const partStyle = css`
    .part-style {
        background-color: #c4c4c4;
    }
`;