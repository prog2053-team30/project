window.MyAppGlobals = {
    clientURL: 'http://localhost:8080/',
    rootPath: '/',
    serverURL: 'http://localhost:8081/',
    staticInfoPath: '/static/staticSiteInfo.json',
    staticInfoPromise: {}, // Variable for static site info promise
    staticInfoData: {}, // Variable for storing fetched static site info.
    username: {
        minlength: 3,
        maxlength: 40,
        validCharacters: "a-zA-Z0-9"
    },
    password: {
        minlength: 8,
        maxlength: 50,
        validCharacters: "\x20-\x7E"
    }
};