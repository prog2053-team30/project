-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Nov 30, 2020 at 04:27 PM
-- Server version: 10.5.8-MariaDB-1:10.5.8+maria~focal
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prog2053-proj`
--
CREATE DATABASE IF NOT EXISTS `prog2053-proj` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `prog2053-proj`;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cid` bigint(8) NOT NULL,
  `post` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `comment` varchar(20000) COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `modDeleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cid`, `post`, `user`, `comment`, `date`, `modDeleted`) VALUES
(5, 20, 25, 'I AGREEE!!!\r\n', '2020-11-24 21:49:27', 0),
(6, 11, 25, 'Why would you say that?', '2020-11-30 14:59:38', 0),
(7, 13, 25, 'Yeah, like do they all have to be part of it?', '2020-11-30 15:00:39', 0),
(8, 21, 1, 'Yo bro, you should not choose a coin strictly because of the logo.\r\nYou know the coins are not real, right? There are no actual coins with dogs on them anywhere.\r\nThey are only a node on a ledger...\r\n\r\nAnyway, if you still only want coins with cute animals, then maybe look into coins like DogeCash, LiteDoge or MonaCoin', '2020-11-30 15:12:05', 0),
(9, 19, 25, 'You would not be able to do that, but if you are still curious, check out the \"what if #1\" by xkcd', '2020-11-30 15:15:10', 0),
(10, 16, 25, 'Bruh, this does not make any sense....', '2020-11-30 15:15:48', 0),
(11, 18, 25, 'Wow, yeah... that was stupid', '2020-11-30 15:19:15', 0),
(13, 23, 0, 'Sorry bro, currently working through a backlog, might take a few days :D', '2020-11-30 15:25:13', 0),
(14, 24, 25, 'aaaaaaaaaaaaaa', '2020-11-30 15:41:39', 0),
(15, 25, 25, 'delte me 2', '2020-11-30 15:42:05', 1),
(16, 10, 0, 'Wow, I must have been really high when I made its post... sorry?', '2020-11-30 15:57:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `commentVote`
--

CREATE TABLE `commentVote` (
  `voteID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `commentID` int(11) NOT NULL,
  `value` int(11) NOT NULL COMMENT '0 is down, 1 is up'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commentVote`
--

INSERT INTO `commentVote` (`voteID`, `userID`, `commentID`, `value`) VALUES
(3, 25, 5, 1),
(4, 25, 6, -1),
(5, 1, 8, 1),
(6, 0, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pid` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `topic` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_bin NOT NULL,
  `content` varchar(20000) COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `modDeleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pid`, `user`, `topic`, `title`, `content`, `date`, `modDeleted`) VALUES
(6, 25, 1, 'Lorem ipsum', 'So manny letters and wordthat says things that are relevant', '2020-08-10 13:52:16', 0),
(7, 29, 1, 'dolor sit amet', 'Laudantium unde inventore rem laudantium. Corporis doloribus veritatis sint modi ratione quidem non. Officiis molestias hic corrupti dolor omnis totam esse voluptatem.\r\n\r\nA aperiam ex ea et voluptas facilis. Qui aut itaque deleniti. Est rerum deleniti dolores laboriosam totam blanditiis eum tempore. Voluptatem adipisci consequatur totam possimus inventore dolor nihil at. Eos non ipsa aut placeat est molestias odio.\r\n\r\nIpsa qui architecto quaerat enim vero ut iusto. Molestiae ducimus quae est iusto. Optio perferendis rerum nihil eos. Molestias nam voluptas dicta perferendis debitis et commodi. Omnis dolores aspernatur autem laborum ut et maxime.\r\n\r\nAliquid consequatur unde nemo. Molestiae sed est non enim. Distinctio quod autem recusandae temporibus alias quo.', '2020-08-18 13:52:16', 0),
(10, 0, 1, 'A aperiam ex ea et voluptas facilis.', 'A aperiam ex ea et voluptas facilis.A aperiam ex ea et voluptas facilis.A aperiam ex ea et voluptas facilis.', '2020-08-30 13:54:14', 0),
(11, 1, 1, 'Data is king', 'Digital data about users is now the new oil. ', '2020-09-05 13:54:14', 0),
(13, 22, 1, 'Letters and why we care', 'So many parts of the alphabet consists of letters,\r\nit\'s kina wierd', '2020-09-22 13:56:18', 0),
(16, 29, 1, 'Vero iure unde nihil nihil blanditiis nisi. ', 'Aut non ut quasi consequatur. Est ducimus vitae mollitia earum quia quidem sit dicta.', '2020-09-23 13:58:28', 0),
(17, 25, 1, 'Newly washed', 'Tables 1 and 4 in the cafeteria are washed. Don\'t use any of the other tables!', '2020-10-03 13:58:28', 0),
(18, 22, 1, 'how to wash a sandblaster, and why you should leave it to the proffesionals', 'I once tried to whash my sandblaster with a gardenhose, and it did not worka t all! The water gunked up the mechanism and brok the whole damn thing!', '2020-10-03 13:56:18', 0),
(19, 22, 1, 'what would happen if you threw a baseball faster than the speed of light?', 'random text that i dont care about', '2020-11-04 13:56:18', 0),
(20, 22, 1, 'turtles should be able to vote!', 'I like turtles', '2020-11-18 13:56:18', 0),
(21, 25, 2, 'DogeCoin is the new Bitcoin!!', 'I the past few weeks I have been researching which cryptocurrency is the best for my business.\r\nAfter lots of tests, I have come to the conclusion that the altcoin Dogecoin is the only one we will accept going forward.\r\nI am not sure if it is very secure or whether the value is estimated to rise or fall, but the coins have cute doggos on them and that is the only thing I care about!\r\n\r\nAre there any other coins that have cute animals on them?', '2020-11-30 15:06:12', 0),
(23, 25, 1, 'Why does it take so logn to become a moderator here?', 'I sent my request minutes ago and still no response.  Seems like the admin is pretty lazy', '2020-11-30 15:23:09', 0),
(24, 25, 4, 'test post to be deleted', 'test', '2020-11-30 15:41:29', 1),
(25, 25, 4, 'test 2', 'delete me!', '2020-11-30 15:41:58', 1),
(26, 29, 9, 'I am greg and I like to plantstuff in my garden', 'I make a youtube series where I plant stuff in my garden.     just search for \"gardening with greg\" and you will find a playlist with all of the episodes I made.   \r\nI will try to keep this topic up to date with my newest additions. if you have any questions please ask and I will try to answer :D', '2020-11-30 16:11:28', 0),
(27, 29, 3, 'Darth Plagueis The Wise', ' Did you ever hear the tragedy of Darth Plagueis The Wise? I thought not. It’s not a story the Jedi would tell you. It’s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life… He had such a knowledge of the dark side that he could even keep the ones he cared about from dying. The dark side of the Force is a pathway to many abilities some consider to be unnatural. He became so powerful… the only thing he was afraid of was losing his power, which eventually, of course, he did. Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic. He could save others from death, but not himself.', '2020-11-30 16:13:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `postVote`
--

CREATE TABLE `postVote` (
  `voteID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `postID` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `postVote`
--

INSERT INTO `postVote` (`voteID`, `userID`, `postID`, `value`) VALUES
(3, 25, 6, -1),
(4, 0, 6, 1),
(5, 29, 6, 1),
(6, 22, 6, 1),
(7, 1, 11, 1),
(8, 0, 11, 1),
(9, 25, 11, 1),
(10, 29, 11, 1),
(11, 25, 20, 1),
(12, 25, 19, -1),
(13, 25, 13, 1),
(14, 25, 21, 1),
(15, 25, 16, -1),
(16, 0, 23, -1);

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `topicID` int(11) NOT NULL,
  `user` int(11) NOT NULL COMMENT 'the user that made the topic',
  `title` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'the title of the topic',
  `createDate` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'date of topic creation.\r\nauto set',
  `description` varchar(280) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`topicID`, `user`, `title`, `createDate`, `description`) VALUES
(1, 0, 'General', '2020-07-12 11:30:52', 'General topic stuff'),
(2, 1, 'Crypto Currencies ', '2020-07-12 11:33:17', 'All about Bitcoin, Etherium, DogeCoin and Bitconnect'),
(3, 1, 'TXT memes and Copypasta', '2020-07-12 11:36:06', 'So much pasta'),
(4, 22, 'Woodworking', '2020-07-12 11:49:37', 'How to and not to cut, saw and turn wood.'),
(5, 25, 'CreepyPasta', '2020-07-12 11:37:53', 'So much creepy stories'),
(6, 0, 'Programming', '2020-07-12 11:39:36', 'How to and not to Code and everything in between'),
(7, 0, 'Ask Anything', '2020-07-12 11:39:36', 'Ask anyone anything'),
(8, 0, 'PC building', '2020-07-12 11:40:54', 'PCMR for life!'),
(9, 29, 'Gardening with greg', '2020-07-12 11:49:37', 'Topic dedicated to gardening with Greg'),
(10, 1, 'Doomsday', '2020-07-12 11:43:12', 'WE ARE ALL GONNA DIE AND I HAVE POPCORN!!'),
(11, 22, 'CORONA', '2020-07-12 11:43:12', 'All you \'NEED\' to know ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` bigint(8) NOT NULL,
  `username` varchar(40) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userType` enum('admin','moderator','user') COLLATE utf8_bin NOT NULL DEFAULT 'user' COMMENT 'the role on the forum',
  `picture` varchar(140) COLLATE utf8_bin DEFAULT NULL COMMENT 'URL to image',
  `email` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `modRequest` enum('no','sent','rejected','') COLLATE utf8_bin NOT NULL DEFAULT 'no',
  `userCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `banned` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `userType`, `picture`, `email`, `modRequest`, `userCreated`, `banned`) VALUES
(0, 'Ferrero', '$2a$10$bBGnmBMyrx5xFXzkbmN0o.vPFdyGyFO7RslcnSJHq0g.QzxVI18DS', 'admin', 'http://localhost:8081/uploads/29f73326e2726fc14ee3027a243ff27c', '', 'no', '2020-07-20 00:00:00', 0),
(1, 'Rocher', '$2a$10$Mi9SQSfgNKIXH0ZjpYe6her7uItniV5NW5CJrmwfDxHGraRmL3eIC', 'moderator', '', '', 'no', '2020-07-20 00:00:00', 0),
(22, 'DonalDuck', '$2a$10$o.rRlxf2leKvdoGEpYuRA.I6kEEpX0EKgPOiuty9SldM4Og10XB3u', 'user', '', '', 'no', '2020-07-20 00:00:00', 0),
(25, 'james', '$2a$10$RLzK68EW.7qB6E5jyIB4pOuAD.CSndRehbm6iRjC9F2TE/aXOYD6i', 'user', 'http://localhost:8081/uploads/673266295d78f4f7243e93f881d84281', '', 'sent', '2020-07-20 00:00:00', 0),
(29, 'greg', '$2a$10$jCVPd4liPYlYqbyarbNWU.S4E3s/Mec2oK5a.QSb16.4cI4F2kzf6', 'user', 'http://localhost:8081/uploads/9399f3bc52ba1a24e67cb2294359d5be', '', 'no', '2020-07-20 11:45:32', 0),
(30, 'qwerty', '$2a$10$.QwJW19dWFZPz9Uw8CrYSOm3oRl3cEdJz051yjG9KOYUEO2BvluRK', 'user', 'http://adrrsse.com/pudding.png', '', 'no', '2020-07-20 09:53:16', 0),
(33, 'creativeUsername', '$2a$10$7CS1Q9hSuhCxRinshUHN2ejQl1C2DWWZA5w/GIKiStDwpEWSvKw/y', 'user', '', '', 'no', '2020-11-30 16:25:29', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `user` (`user`),
  ADD KEY `post` (`post`);

--
-- Indexes for table `commentVote`
--
ALTER TABLE `commentVote`
  ADD PRIMARY KEY (`voteID`),
  ADD KEY `user` (`userID`) USING BTREE,
  ADD KEY `comment` (`commentID`) USING BTREE;

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `user` (`user`),
  ADD KEY `topicID` (`topic`) USING BTREE;

--
-- Indexes for table `postVote`
--
ALTER TABLE `postVote`
  ADD PRIMARY KEY (`voteID`),
  ADD KEY `user` (`userID`) USING BTREE,
  ADD KEY `post` (`postID`) USING BTREE;

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`topicID`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `commentVote`
--
ALTER TABLE `commentVote`
  MODIFY `voteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `postVote`
--
ALTER TABLE `postVote`
  MODIFY `voteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `topicID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post`) REFERENCES `posts` (`pid`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
