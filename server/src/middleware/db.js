import mysql from 'mysql';

let db = mysql.createPool({
    connectionLimit : 100,
    host: "db",
    user: "admin",
    password: "password",
    database: "prog2053-proj"
});



db.getUserByID = (id , callback) => {
    process.nextTick(()=> {
        let q = "SELECT * FROM `users` WHERE uid = ?";
        db.query(q, [id], (err, result )=> {
            if (err) console.error(err);
            else callback(null,result[0]);
        })
    });
};

db.getUserByUsername = (username , callback) => {
    process.nextTick(()=> {
        let q = "SELECT * FROM `users` WHERE username = ?";
        db.query(q, [username], (err, result )=> {
            if (err) console.error(err);
            else callback(null,result[0]);
        })
    });
};



/**
 * Checks whether or not there exists a user with that username and send result to callback
 *
 * This function will try to asyncronusly connect to the databse and check for a user with that username.
 * If the result of the query s
 * @param name
 * @param callback
 */
db.checkUserExistence = (name, callback)=>{
    db.query("SELECT * FROM `users` WHERE `username` = ? ",[name], (err, result) =>{
        if (err) throw err;
        callback(result.length !== 0);
    });
};

db.checkTopicExistence = (Title, callback)=>{
    db.query("SELECT * FROM `topic` WHERE `title` = ? ",[Title], (err, result) =>{
        if (err) throw err;
        callback(result.length !== 0);
    });
};


db.countTopics = (callback)=>{
    db.query("SELECT COUNT(*) as topic_count FROM `topic`", (err,resTopicCount) => {
        if (err){console.error(err);}
        else{
            callback(resTopicCount[0].topic_count);
        }
    });
};

db.countPosts = (callback)=>{
    db.query("SELECT COUNT(*) as post_count FROM `posts` WHERE posts.modDeleted = 0", (err,resPostCount) => {
        if (err){console.error(err);}
        else{
            callback(resPostCount[0].post_count);
        }
    });
};

// sitewide count of comments. Probably never useful....
db.countComments = (callback)=>{
    db.query("SELECT COUNT(*) as comment_count FROM `comments` where comments.modDeleted = 0", (err,resCommentCount) => {
        if (err){console.error(err);}
        else{
            callback(resCommentCount[0].comment_count);
        }
    });
};



export {db}