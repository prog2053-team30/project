import passport from 'passport';
import {db} from "./index.js";
import {strat} from "../config/index.js";



passport.use(strat);
passport.serializeUser(function(user, cb) {
    cb(null, user.uid);
});

passport.deserializeUser(function(id, cb) {
    db.getUserByID(id, function (err, user) {
        if (err) { return cb(err); }
        cb(null, user);
    });
});

/**
 * Checks wether the user is authenticated, will not let other middleware resolve if not authenticated
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function requireAuthentication(req,res,next){
    if(req === undefined || res === undefined || next === undefined){
        console.log("req auth undefined!!!");
        console.log("If  you see this message,\n" +
            "You probably tried to add this as a middleware with 'requireAuthentication()'.\n" +
            "DON'T do that! Instead write 'requireAuthenticatin' without the parenthesis. \n" +
            "If that's not hte problem, then contact your admin.\n" +
            "To the Admin:\n" +
            "Dear Admin\n" +
            "This is a middleware for passport to ensure that the connected session user is authenticated.\n " +
            "It is only tested and verified to be used with express, passport and passport local strategy.\n" +
            "please refrain from using it if you don't know how it works, or you use other software than the supported once.\n" +
            "Sincerely\n" +
            "That guy who wrote this function");
        throw "error";
    }

    if (req.isAuthenticated()){
        return next()
    }else {
        res.status(401).json({"msg":"ERROR! Not authenticated!"})
    }
}

export {
    passport as passport,
    requireAuthentication
}