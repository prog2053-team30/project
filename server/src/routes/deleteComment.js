import Router from 'express';
import bodyparser from 'body-parser';
import {db, requireAuthentication} from '../middleware/index.js';

const router = Router();

/*
 * Route for users to delete/block comments
 *
 * This route can be used by any user to delete their own comments, or by mods/admin to block other users comments.
 * Users can only delete their own comments, but all mods can block all non-mods/non-admin comments.
 * Admins are the only once able to block comments made by mods.
 * the difference between blocked and deleted is that deleted comments are gone for ever and blocked comments can still
 * be viewed by admins/mods on the admin/modpage.
 * If an admin or a mod tries to remove their own comment, it will always be deleted and not blocked.
 *
 * Required parameters are only a valid comment id sent as {cid : <value>}
 */

router.post('/deleteComment', requireAuthentication, bodyparser.json(), (req, res) => {
    // capture input from user
    let commentID = Math.abs(Number(req.body.cid));
    //validate input
    if(isNaN(commentID)){
        return res.status(400).json({msg:"Not a valid comment id"});
    }
    // check if user can delete the specified comment
    db.query("SELECT comments.`cid`, comments.`post`, comments.`user`, comments.`comment`, comments.`date`, comments.`modDeleted`, (SELECT userType from users where users.uid = comments.user) as userType from comments WHERE  cid = ?", [commentID], (err,result)=>{
       if (err) throw err;
       if (result.length < 1){
           //if there does not exist a comment with that id, respond 400 and end
           return res.status(400).json({msg:"Comment does not exist"});
       } else if(result[0].user == req.user.uid){
           // if there exists a comment with that id, check if user owns it
           // then if true tell the database to delete it
            db.query("DELETE FROM `comments` WHERE `comments`.`cid` = ?", [commentID], (err,result) =>{
                if (err) throw err;
                // then tell the database to delete all associated votes
                db.query("DELETE FROM `commentVote` WHERE `commentVote`.`commentID` = ?", [commentID], (err,result) => {
                    if(err) throw err;
                    // respond that all went well
                    return res.status(200).json({msg:"Comment deleted"});
               });
           });
       } else if(
           (req.user.userType !== "user" && result[0].userType === "user")||
           (req.user.userType === "admin" && result[0].userType === "moderator")
       ){
           // if the user did not own the comment, check if they are  either a mod/admin and the comment was made by a user
           // or if the user is an admin and the comment was made by a moderator
           // then if true: update the status of the comment to blocked by a moderator
           db.query("UPDATE comments SET modDeleted = 1 WHERE cid = ?", [commentID], (err,result) =>{
                if (err) throw err;
                // respond that all went well
                return res.status(200).json({msg:"Comment blocked"});
           });
       } else {
           // respond that the user is not allowed to remove this comment
           return res.status(400).json({msg:"You can't remove this comment!"});
       }
    });

});

export { router as deleteComment };
