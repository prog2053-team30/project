import Router from 'express';
import {db, requireAuthentication} from "./../middleware/index.js";
import bodyparser from 'body-parser';
const router = Router();

// Fetch data for single topic
router.post('/topic',requireAuthentication, bodyparser.json(), (req, res) => {
    let topicID = Math.abs(Number(req.body.tid));
    let sort = req.body.sort==="score"?"vote_score":"posts.date";
    let increment = Math.abs(Number(req.body.increment));
    let start =Math.abs( Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment) || isNaN(topicID)){return res.status(400).json({"msg":"send a vlaid request! "})}


    // get data about score
    db.query("SELECT topic.`topicID`, topic.`title`, topic.`createDate`, topic.`description`, IFNULL(COUNT(posts.pid),0) AS post_count FROM topic LEFT JOIN posts ON topic.topicID = posts.topic WHERE topicID = ? AnD posts.modDeleted = 0",[topicID],(err, topicDATA) => {
        if(err) throw err;
        if (topicDATA.length === 0) return res.status(400).json({"msg":"no topic with that id :" + topicID});

        let q = "SELECT \
                    posts.pid,\
                    posts.topic,\
                    posts.title,\
                    (SELECT users.username FROM users WHERE users.uid = posts.user) as username,\
                    posts.date,\
                    (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.post = posts.pid AND comments.modDeleted = 0) AS comment_count,\
                    (SELECT IFNULL(SUM(postVote.value),0) FROM postVote WHERE postVote.postID = posts.pid) AS vote_score\
                FROM\
                    posts\
                WHERE\
                    posts.topic = ?\
                    AND\
                    posts.modDeleted = 0\
                ORDER BY\
                    " + sort + " DESC, \
                    posts.pid DESC\
                LIMIT ?, ?";
        db.query(q,[topicID,start,(increment)],(err, AllThePostsInThisTopic) => {
            if (err) throw err;
            res.status(200).json({
                topicID: topicDATA[0].topicID,
                title:topicDATA[0].title,
                description: topicDATA[0].description,
                date: topicDATA[0].date,
                posts:{
                    total:topicDATA[0].post_count,
                    ref:start,
                    post:AllThePostsInThisTopic
                }

            });
        });
    });
})

export { router as topic };