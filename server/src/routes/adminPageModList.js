import Router from 'express';
import {db, requireAuthentication} from "../middleware/index.js";
import bodyparser from 'body-parser';


const router = Router();

/*
 * Route for getting a list of all mods.
 *
 * This list is used exclusively on the admin/moderator page and is only available if the user is an admin.
 *
 * When requested, 2 parameters are mandatory, a reference and an increment.
 * These variables are used for pagination on the frontend module to reduce the number of entries per request.
 * ref and increment each reffer to the number of the results we want from the database, ref beeing the start value and
 * increment beeing the increment of how many we want.
 */
router.post('/adminPageModList',requireAuthentication, bodyparser.json(),function (req, res) {
    //capture vaules from the request
    let increment = Math.abs(Number(req.body.increment));
    let start =Math.abs( Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment)){return res.status(400).json({"msg":"send a valid request!!!!"})}

    //only admins are allowed to perform this request, stop anyone else and send them a message.
    if(req.user.userType !== "admin"){
        return res.status(401).json({"msg": "You are not allowed!"});
    }

    //count all users that exist
    db.query("SELECT (select IFNULL(COUNT(users.uid),0) from users where  users.userType = 'moderator') as user_count from users", (err, userCount) => {
        if (err) throw err;


        //get all users with count of comments and posts, sorted by usertype and username
        let q = "SELECT users.username, users.uid, users.userCreated, users.userType, (SELECT IFNULL(COUNT(posts.pid),0) FROM posts WHERE posts.user = users.uid) AS post_count, (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.user = users.uid) AS comment_count FROM users WHERE users.userType = 'moderator' GROUP BY 1 ORDER BY users.userType ASC, LOWER(users.username) DESC LIMIT ?, ?";
        db.query(q,[start, increment],(err, result)=>{
            if (err)throw err;
            //respond with the data requested
            res.json({
                UserCount:userCount[0].user_count,
                ref:start,
                users:result
            });
        });
    });

});

export { router as adminPageModList};