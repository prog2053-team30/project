import Router from 'express';
import {requireAuthentication} from './../middleware/index.js'
const router = Router();

router.post('/profile', requireAuthentication,(req, res) => {
    let tmp = req.user;
    delete tmp.password;
    res.status(200).json(tmp);

});

export { router as profile };