import Router from 'express';
import bodyparser from 'body-parser';
import {db, requireAuthentication} from '../middleware/index.js';

const router = Router();
/*
 * Route for banning a user
 *
 * This route is used to ban a user using their uid.
 * Only admins and moderators can ban users and only admins can ban moderators.
 *
 * The required parameters for this route are a valid userid of an existing user
 */
router.post('/banUser', requireAuthentication, bodyparser.json(), (req, res) => {
    let userToBlock = Math.abs(Number(req.body.uid));
    //validate input
    if(isNaN(userToBlock)){return res.status(400).json({msg:"send a valid input"})}

    //is the person trying to ban authorized?
    if(req.user.userType !=="moderator" && req.user.userType !== "admin"){
        return res.status(403).json({msg:"sorry! you are not authorized to ban anyone"})
    }


    //does user exist? what kind of user is it we want to delete?
    db.query("SELECT * FROM users WHERE users.uid = ?", [userToBlock], (err, result) =>{
        if (err)throw err;
        //did we get a result?
        if (result.length < 1 ){
            // no we did not, let's end this
            return res.status(404).json({msg:"user not found"});
        }else if(
            (req.user.userType === "moderator" && result[0].userType === "user") ||
            (req.user.userType === "admin"     && result[0].userType !== "admin")
        ) {
            //if you are a mod and want to ban a useer or if you are a admin and want to ban someone that's not an admin:
            // then go ahead :D
            db.query("UPDATE users SET users.banned = 1 WHERE uid = ?", [userToBlock],(err,resultOfBan)=>{
                if (err) throw err;
                return res.status(200).json({msg:"users blocked"});
            });

        }else{
            return res.status(403).json({msg:"Sorry, you can't ban this users"});
        }
    });
});

export { router as banUser };
