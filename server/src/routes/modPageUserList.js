import Router from 'express';
import {db, requireAuthentication} from "../middleware/index.js";
import bodyparser from 'body-parser';


const router = Router();

router.post('/modPageUserList',requireAuthentication, bodyparser.json(),function (req, res) {
    let increment = Math.abs(Number(req.body.increment));
    let start =Math.abs( Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment)){return res.status(400).json({"msg":"send a valid request!!!!"})}

    if(req.user.userType === "user"){
        return res.status(401).json({"msg": "You are not allowed!"});
    }

    //count all users
    db.query("SELECT IFNULL(COUNT(uid),0) as user_count from users", (err, userCount) => {
        if (err) throw err;


        //get all users with count of comments and posts, sorted by userstype and username
        let q = "SELECT users.username, users.uid, users.userCreated, users.userType, (SELECT IFNULL(COUNT(posts.pid),0) FROM posts WHERE posts.user = users.uid) AS post_count, (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.user = users.uid) AS comment_count FROM users GROUP BY 1 ORDER BY users.userType ASC, LOWER(users.username) ASC LIMIT ?, ?";
        db.query(q,[start, increment],(err, result)=>{
            if (err)throw err;
            res.json({
                UserCount:userCount[0].user_count,
                ref:start,
                users:result
            });
        });
    });

});

export { router as modPageUserList};