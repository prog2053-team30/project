import Router from 'express';
import {db, requireAuthentication} from '../middleware/index.js';
import bodyparser from 'body-parser';
const router = Router();


/*
 * Route for getting all the posts
 *
 * This route will return a list of all posts, who created the post, a count of all comments and score.
 *
 * When requested, 3 parameters are mandatory, a reference and a increment for pagination,
 * and sort value which should be used to choose which method to sort the results.
 *
 */
router.post('/allPosts',requireAuthentication, bodyparser.json(), (req, res) => {
    // get parameters from the request
    let increment = Math.abs(Number(req.body.increment));
    let sort = req.body.sort==="score"?"vote_score":"posts.date";
    let start =Math.abs( Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment)){return res.status(400).json({"msg":"send a valid request!!!!"})}

    //count all posts
    db.countPosts((postCount)=>{
        // query a list of all posts from the database, with aditional data such as user info, count of comments
        // and a post score
        db.query("SELECT posts.pid, posts.topic, posts.title, posts.date, posts.modDeleted, posts.user, (SELECT users.username FROM users WHERE users.uid = posts.user)as username, (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.post = posts.pid AND comments.modDeleted = 0) AS comment_count, (SELECT IFNULL(SUM(postVote.value),0) FROM postVote WHERE postVote.postID = posts.pid) AS vote_score FROM posts WHERE posts.modDeleted = 0 GROUP BY 1 ORDER BY "+sort+" DESC LIMIT ?, ?", [start, increment], (err,resPostsGroup)=>{
            if (err){console.error(err);}
            else{
                //respond with requested data
                res.status(200).json({
                    total: postCount,
                    ref: start,
                    post: resPostsGroup
                });
            }
        });
        }
    )
});
    


export {router as allPosts};
