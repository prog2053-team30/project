import Router from 'express';
import {db,requireAuthentication} from "../middleware/index.js";
import multer from "multer";
/* TODO: More relevant imports? */
const upload = multer();
const router = Router();

router.post('/newComment', upload.none(), requireAuthentication, (req, res) => {
    // TODO: Handle requests about the creation of comments

    let postID = Number(req.body.pid);
    let comment = req.body.comment;

    if(comment.length < 3 || comment.length >= 20000){
        return res.status(400).json({"msg":"bad request: data in wrong format"});
    }

    db.query("INSERT INTO `comments` (`cid`, `post`, `user`, `comment`, `date`, `modDeleted`) VALUES (NULL, ?, ?, ?, current_timestamp(), '0');", [postID,req.user.uid,comment], (err,result)=>{
        if(err) throw err;
        res.json({
            pid: postID,
            cid: result.insertId
        });
    })
});

export { router as newComment };