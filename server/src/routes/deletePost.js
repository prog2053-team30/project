import Router from 'express';
import bodyparser from 'body-parser';
import {db, requireAuthentication} from '../middleware/index.js';

const router = Router();

/*
 * Route for users to delete/block posts
 *
 * This route can be used by any user to delete their own posts, or by mods/admin to block other users posts.
 * Users can only delete their own posts, but all mods can block all non-mods/non-admin posts.
 * Admins are the only one able to block posts made by mods.
 * The difference between blocked and deleted is that deleted posts are gone for ever and blocked posts can still
 * be viewed by admins/mods on the admin/modpage.
 * If an admin or a mod tries to remove their own comment, it will always be deleted and not blocked.
 *
 * Required parameters are only a valid post id sent as {pid : <value>}
 */



router.post('/deletePost', requireAuthentication, bodyparser.json(), (req, res) => {
    // capture input from user
    let postID = Math.abs(Number(req.body.pid));
    //validate input
    if(isNaN(postID)){
        return res.status(400).json({msg:"Not a valid inut"});
    }
    //check if user can delete the spesified post
    db.query("SELECT posts.`pid`, posts.`user`, posts.`topic`, posts.`title`, posts.`content`, posts.`date`, posts.`modDeleted` ,(SELECT userType from users where users.uid = posts.user) as userType FROM posts WHERE pid = ?", [postID], (err,result)=>{
        if(err) throw err;
        if (result.length < 1){
            //if there does not exist a post with that id, respond 400 and end
            return res.status(400).json({msg:"Post does not exist"});
        }else if(result[0].user == req.user.uid){
            // if there exists a post with that id, check if user owns it
            // then if true tell the database to delete all comments attached to that post

            db.query("DELETE FROM comments where comments.post = ?",[postID],(err, CommentsDELETEresult)=>{
                if(err)throw err;
                // then delete the post itself
                db.query("DELETE FROM `posts` WHERE `posts`.`pid` = ?", [postID], (err,result) =>{
                    if (err) throw err;
                    // then delete the post votes
                    db.query("DELETE FROM `postVote` WHERE `postVote`.`postID` = ?", [postID], (err,result) => {
                        if(err) throw err;
                        // then respond that all went well
                        return res.status(200).json({msg:"Post deleted"});
                    });
                });
            })
        }else if(
            (req.user.userType !== "user" && result[0].userType === "user")||
            (req.user.userType === "admin" && result[0].userType === "moderator")
        ){

            // if the user did not own the post, check if they are  either a mod/admin and the comment was made by a user
            // or if the user is an admin and the post was made by a moderator
            // then if true: update the status of the post to blocked by a moderator
            db.query("UPDATE posts SET modDeleted = 1 WHERE pid = ?", [postID], (err,result) =>{
                if (err) throw err;
                // respond that all went well
                return res.status(200).json({msg:"Post blocked"});
            });
        } else {
            // respond that the user is not allowed to remove this post
            return res.status(400).json({msg:"You can't remove this post!"});
        }
    });
});

export { router as deletePost };