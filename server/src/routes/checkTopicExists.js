import Router from 'express';
import {db} from "../middleware/index.js";

const router = Router();

/*
 * Route to check if a topic exists with a a spesific name
 *
 * This route checks if the topic title the user sent, exists.
 *
 * required parameter for this route is a
 */
router.get('/checkTopicExists',function (req, res) {
    // use dedicated function to check if topic exists with this title
    db.checkTopicExistence(req.query.title, (bool) =>{
        if (bool) {// respond
            res.status(200).send("true");
        }else{
            res.status(200).send("false")
        }
    });
});

export { router as checkTopicExists};