import Router from 'express';
import multer from 'multer';
import { db, requireAuthentication } from '../middleware/index.js';

const router = Router();
const upload = multer();

router.post('/updateComment',requireAuthentication, upload.none(), (req, res) => {
    let commentID = Math.abs(Number(req.body.cid));
    let comment = req.body.newComment;

    req.on('data', chunk => {
        if ( req.body.length > 1e6) {
            return req.connection.destroy();
        }
    });

    //check if values too long
    if(comment.length > 20000|| isNaN(commentID)){return res.status(400).json({"msg":"Send a valid request!!!!"})}

    //check if user owns comment
    db.query("SELECT * FROM comments WHERE comments.cid = ?", [commentID], (err,result)=>{
        if (err) throw err;
        if(result.length >0){
            if (result[0].user === req.user.uid){
                db.query("UPDATE comments SET comments.comment = ? WHERE comments.cid = ?",[comment,commentID],  (err,result2)=>{
                    if (err) throw err;
                    res.status(200).json({msg:"Updated comment :"+ commentID});
                });
            }else{
                res.status(403).json({msg:"You don't own comment "+ commentID});
            }
        }else {
            res.status(400).json({msg:"No comment with id"+ commentID});
        }
    });

});

export { router as updateComment };