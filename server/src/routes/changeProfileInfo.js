import Router from 'express';
import {requireAuthentication,db} from './../middleware/index.js'
import multer from 'multer';
let upload = multer();
const router = Router();

/*
 * Route for changing profile information
 *
 * This route allows logged in users to change their username and or email (profilepicture and password is another route)
 * users can only change their own information and the new information has to be valid, no user can change their
 * username to be equal to some other users username.
 *
 * Required parameters are username and email sent as multipart formdata.
 *
 */


router.post('/changeProfileInfo', requireAuthentication, upload.none(), (req, res) => {
    // get input from request
    let inputUsername = req.body.username;
    let inputEmail = req.body.email;

    //define regex taht can be used to validate username and email
    let usernameRegex = RegExp(`^[a-zA-Z0-9]{3,40}$`)
    let emailRegex = RegExp("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");

    // set default values for username and email.
    // these are the varaibles that will be used to update the user in the database.
    // they will be updated with either the existing username and email or
    // new data if the new data is existing and valid.
    let okUsername = "";
    let okEmail = "";


    // check whether the username is in use by another user
    db.checkUserExistence(inputUsername,(bool) => {
        // Check username if valid (not empty, not undefined, not used, ok regex)
        if (!(inputUsername === "" ||
            inputUsername === undefined ||
            inputUsername === null||
            bool ||
            inputUsername.length < 3 ||
            inputUsername.length > 40) &&
            usernameRegex.test(inputUsername)
        ) {
            okUsername = inputUsername;
        } else { // if new username not valid, use the old one
            okUsername = req.user.username;
        }
        //check if email valid (not empty, not undefined, ok regex)
        if (!(inputEmail === "" ||
            inputEmail === undefined ||
            inputEmail === null ||
            inputEmail.length > 127) &&
            emailRegex.test(inputEmail)
        ) {
            okEmail = inputEmail;
        } else { // if the email is not valid use the old one
            okEmail = req.user.email;
        }
        //update the user with ok information (which is either the old existing validated information or new and validated )
        db.query("UPDATE `users` SET `username`= ?, `email` = ? WHERE uid = ? ", [okUsername, okEmail, req.user.uid], (err, resultat) => {
            if (err) throw err;
            // respond with an 200 ok when the query completes with no errors
            res.status(200).json({"msg": "Update Done"});
        });
    });
});

export { router as changeProfileInfo };