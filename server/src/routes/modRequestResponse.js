import Router from 'express';
import { db, requireAuthentication } from '../middleware/index.js';

const router = Router();

router.use(Router.json());

router.post('/modRequestResponse', requireAuthentication, function(req, res) {
    let userID = Math.abs(Number(req.body.uid));
    let approve = req.body.approve;

    //validate input
    if(isNaN(userID)){
        return res.status(400).json({ms:"Invalid input!!"});
    }

    if(req.user.userType !== "admin"){
        return res.status(403).json({msg:"you are not admin!!!"});
    }
    db.query("UPDATE `users` SET `userType` = ?, `modRequest`=? WHERE `users`.`uid` = ?", [ approve?"moderator":"user",approve?"no":"rejected", userID],(err,response)=>{
        if(err) throw err;
        res.status(200).json({msg: `make ${userID} mod? ${approve}`});
    });


});

export { router as modRequestResponse};