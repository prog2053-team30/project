import Router from 'express';
import {db, requireAuthentication} from "../middleware/index.js";
import bodyparser from 'body-parser';


const router = Router();

router.post('/requestMod',requireAuthentication,  (req, res) =>{
    if (req.user.userType === "moderator" ||
        req.user.userType === "admin" ||
        req.user.modRequest === "sent"||
        req.user.banned === 1
    ){
        return res.status(400).json({msg:"you cant send a mod request, sorry!"});
    }

    db.query("UPDATE users SET users.modRequest = 'sent' WHERE users.uid = ? ", [req.user.uid], (err,result)=>{
       if(err) throw err;
       res.status(200).json({msg:"Request sent! Now wait patiently!"});
    });

});

export { router as modRequest};