import Router from 'express';
import {db, requireAuthentication} from "../middleware/index.js";
import multer from 'multer';
const profilePictureupload = multer({dest: "./uploads/"});
const router = Router();

router.post('/profilePictureUpload', profilePictureupload.single("ProfilePictureUpload"), requireAuthentication ,(req, res) => {
    db.query("UPDATE `users` SET `picture` = ? WHERE  `uid` = ?", [("http://localhost:8081/uploads/"+req.file.filename), req.user.uid], (err, result)=>{
        if (err)throw err;
        res.status(200).json({"msg":"Upload : OK"});
    })

});

export { router as profilePictureupload};