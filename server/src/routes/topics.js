import Router from 'express';
import {db, requireAuthentication} from '../middleware/index.js';
import bodyparser from 'body-parser'
const router = Router();


router.post('/topics',requireAuthentication, bodyparser.json(),(req, res) => {
    let increment = Math.abs(Number(req.body.increment));
    let start =Math.abs( Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment)){return res.status(400).json({"msg":"send a valid request!!!!"})}


    //get topic count
    db.countTopics((topicCount)=>{
        //get som topics an the info about it
        db.query("SELECT topic.topicID, topic.title, topic.createDate, (select IFNULL(COUNT(posts.pid),0) FROM posts WHERE posts.topic = topic.topicID and posts.modDeleted = 0)AS post_count FROM topic ORDER BY topicID ASC LIMIT ?,?", [start, (increment)], (err,resTopicsGroup) => {
            if (err){console.error(err);}
            else{
                res.json({
                    total:topicCount,
                    ref:start,
                    topic: resTopicsGroup
                });
            }
        });
        }
    )
});

export { router as topics };