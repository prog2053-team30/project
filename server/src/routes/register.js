import Router from 'express';
import multer from 'multer';
import {db} from '../middleware/index.js';
import bcrypt from 'bcryptjs';
import {BCRYPT_SALT_ROUNDS} from  '../config/bcrypt.js';
const router = Router();
const upload = multer();

router.post('/register',upload.none(), async (req, res) => {
  let username = "";
  let password = "";
  let confim = "";
  let query = "";
  const MINLEN = 3;
  const MAXLEN = 40;
  //check username regex
  const REGEX = RegExp(`^[a-zA-Z0-9]{${MINLEN},${MAXLEN}}$`);

  //checks if request is too big
  req.on('data', chunk => {
    if ( req.body.length > 1e6) {
      return req.connection.destroy();
    }
  });

  username = req.body.username;
  password = req.body.password;
  confim = req.body.passwordConfirm;


  // tests if username is valid
  if(REGEX.test(username) === false){
    return res.status(400).json({"msg":"Client username not valid"});
  }

  //check if username already exists
  db.checkUserExistence(username, (userExists)=>{

    //does this user exists?
    if(userExists){
      return res.status(400).json({"msg":"Client username not valid"});
    }

    //check if password is valid
    if (password !== confim){
      console.log("password is not equal to password-confirm")
      res.status(400);
      return res.json({"msg":"ERROR: PASSWORD missmatch, password not equal to confirm"});
    }

    //hashing password
    const salt = bcrypt.genSaltSync(BCRYPT_SALT_ROUNDS);
    const hash = bcrypt.hashSync(password, salt);

    //performs regitry query
    query = "INSERT INTO `users` (`uid`, `username`, `password`, `userType`, `picture`, `email`,`modRequest`,`userCreated`, `banned`) VALUES (NULL, ?, ?, 'user', '', '','no',CURRENT_TIME(),'0')";
    db.query(query,[username,hash], function (err, result) {
      if (err) { // on error, tell the user
        console.error(err);
        res.status(500).json({"msg":"Error in database"});
        throw err;
      } else {//everything ok? tel the user and log
        res.status(200).json({"msg":"OK"});
        console.debug("Registered user: " + username);
      }
    });
  });



});

export { router as register };