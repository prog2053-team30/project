import Router from 'express';
import multer from 'multer';
import { db, requireAuthentication } from '../middleware/index.js';

const router = Router();
const upload = multer();

router.post('/newTopic',requireAuthentication, upload.none(), (req, res) => {
    let topicTitle = "";
    let topicDesc = "";
    let query = "INSERT INTO `topic` (`topicID`, `user`, `title`, `createDate`, `description`) VALUES (NULL, ?, ?, CURRENT_TIME(), ?)";
    
    // Checks if request is too big
    req.on('data', chunk => {
        if ( req.body.length > 1e6) {
            return req.connection.destroy();
        }
    });
    
    // Get data from the request
    topicTitle = req.body.topic;
    topicDesc = req.body.description;

    //check if values too long
    if(topicTitle.length > 80 || topicDesc.length > 280){return res.status(400).json({"msg":"send a valid request!!!!"})}

    //Check if data is valid
    db.query("SELECT IFNULL(COUNT(topic.topicID),0) AS tc FROM topic WHERE `title` = ? ", [topicTitle], (err, topicCount)=>{
        if (topicCount[0].tc !== 0){
            return  res.status(400).json({"msg":"Topic already exists"})
        }else if(topicTitle === ""){
            return  res.status(400).json({"msg":"Topic title can't be blank"})
        }

        // Handle requests about the creation of new topics
        db.query(query, [req.user.uid, topicTitle, topicDesc], function(err, result) {
            if ( err ) {
                res.status(500).json({msg: "Error in database."});
            } else {
                res.status(200).json({
                    msg: "Added topic",
                    tid: result.insertId
                });
            }
        })
    });
    
    
  

});

export { router as newTopic };