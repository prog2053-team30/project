import Router from 'express';
import {db, requireAuthentication} from './../middleware/index.js';
import bodyparser from 'body-parser';

const router = Router();

router.post('/post', requireAuthentication, bodyparser.json(), (req, res) => {
    let postID = Math.abs(Number(req.body.pid));
    let sort = req.body.sort === "date" ? "comments.date" : "comment_score";
    let increment = Math.abs(Number(req.body.increment));
    let start = Math.abs(Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment) || isNaN(postID)) {
        return res.status(400).json({"msg": "send a valid request!!!!"})
    }

    let q = "SELECT \
                posts.`pid` AS post_id,\
                posts.`user` AS user_id,\
                posts.`topic` AS topic_id,\
                posts.`title`,\
                posts.`content`,\
                posts.`date` AS date_created,\
                posts.`modDeleted`,\
                (SELECT users.userType FROM  users WHERE users.uid = user_id) as userType,\
                (SELECT users.username FROM users WHERE users.uid = user_id) AS username,\
                (SELECT users.picture FROM users WHERE users.uid = user_id) AS picture,\
                (SELECT users.banned FROM  users WHERE users.uid = user_id) as banned,\
                (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.post = posts.pid) AS comments_count,\
                (SELECT SUM(postVote.value) FROM postVote WHERE postVote.postID = posts.pid) AS score,\
                CASE WHEN EXISTS(\
                    SELECT pv.userID FROM postVote AS pv WHERE pv.userID = ? AND pv.postID  = posts.pid AND pv.value = 1\
                        ) THEN 1 WHEN EXISTS(\
                    SELECT pv.userID FROM postVote AS pv WHERE pv.userID = ? AND pv.postID  = posts.pid AND pv.value = -1\
                        ) THEN -1 ELSE 0 END AS DidIVote\
            FROM\
                `posts`\
            WHERE\
                posts.pid = ? AND posts.modDeleted = 0";

    //get info about post
    db.query(q, [req.user.uid, req.user.uid, postID], (err, result) => {
        if (err) throw err;
        if (result.length < 1) {
            return res.status(404).json({msg: "No post with that id", id: postID});
        }

        // get all the info about all the comments
        db.query("SELECT comments.`cid`, comments.`post`, comments.`user`, comments.`comment`, comments.`date`,users.userType, comments.`modDeleted`, users.`username`,users.banned, `users`.`picture`, (select IFNULL(SUM(cv.value),0) FROM commentVote AS cv WHERE cv.commentID = comments.cid )AS comment_score, CASE WHEN EXISTS( SELECT cv.userID FROM commentVote AS cv WHERE cv.userID = ? AND cv.commentID = comments.cid AND cv.value = 1 ) THEN 1 WHEN EXISTS( SELECT cv.userID FROM commentVote AS cv WHERE cv.userID = ? AND cv.commentID = comments.cid AND cv.value = -1 ) THEN -1 ELSE 0 END AS DidIVote FROM `comments` LEFT JOIN users ON users.uid = comments.user LEFT JOIN commentVote ON commentVote.commentID = comments.cid WHERE comments.modDeleted = 0 AND comments.post = ? GROUP BY 1 ORDER BY " + sort + " DESC, comments.cid DESC LIMIT ?, ?", [req.user.uid, req.user.uid, postID, start, (increment)], (err, comments) => {
            if (err) throw err;
            res.status(200).json({
                post_id: postID,
                user_id: result[0].user_id,
                username: result[0].username,
                topic_id: result[0].topic_id,
                userPicture: result[0].picture,
                title: result[0].title,
                date: result[0].date_created,
                score: result[0].score,
                content: result[0].content,
                userType:result[0].userType,
                comment_count: result[0].comments_count,
                didIVote: result[0].DidIVote,
                banned:result[0].banned,
                comments: comments
            });
        });
    });

})

export {router as post};