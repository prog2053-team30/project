import Router from 'express';
import {db} from "../middleware/index.js";

const router = Router();

/*
 * Route to check if a user exists with a certain username
 *
 * Required parameters: username sent as name in a get request
 *
 */

router.get('/checkUserExists', function (req, res) {
    //use dedicated function to check if user exists
    db.checkUserExistence(req.query.name, (bool) =>{
        if (bool) { // respond with an answer
            res.status(200).send("true");
        }else{
            res.status(200).send("false")
        }
    });
});

export { router as checkUserExists};