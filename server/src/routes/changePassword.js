import Router from 'express';
import {requireAuthentication,db} from './../middleware/index.js';
import multer from 'multer';
import bcrypt from "bcryptjs";
import {BCRYPT_SALT_ROUNDS} from "../config/bcrypt.js";
let upload = multer();
const router = Router();

/*
 * Routing for changing a users password
 *
 * Anyone using the platform can request to change their password using this function.
 * They can only change their own password and they need to provide the current password.
 * It is also required to send a confirmation of their new password.
 *
 * All data provided must be sent as multipart-formdata
 */


router.post('/changePassword', requireAuthentication, upload.none(), (req, res) => {
    //capture values from request
    let currentPassword = req.body.currentPassword;
    let newPassword = req.body.newPassword;
    let confirmNewPassword = req.body.confirmNewPassword;
    //validate passwords
    if( currentPassword === undefined ||
        newPassword === undefined ||
        confirmNewPassword === undefined||
        confirmNewPassword !== newPassword ||
        newPassword.length < 8 ||
        newPassword.length > 50){
        return res.status(400).json({msg:"input not valid!!!",current:currentPassword,new:newPassword,confirmNewPassword:confirmNewPassword});
    }

    //check if current password matches the one in the database
    bcrypt.compare(currentPassword,req.user.password).then(result => {
        if (result){

            //generate new password hash
            const salt = bcrypt.genSaltSync(BCRYPT_SALT_ROUNDS);
            const hash = bcrypt.hashSync(newPassword, salt);

            // update password
            db.query("UPDATE users SET users.password = ? WHERE uid = ?", [hash,req.user.uid], (err,result)=>{
                if (err) throw  err;
                // update password for logged in user, this data is stored in a session storage and will not be updated
                // before the user logs in again
                req.user.password = hash;
                res.status(200).json({msg:"update complete"});
            });

        }else{
            // wrong current password.
            res.status(400).json({msg:"current password not correct"});
        }
    });
});

export { router as changePassword };