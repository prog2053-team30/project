import Router from 'express';
import {db, requireAuthentication} from '../middleware/index.js';
import bodyparser from 'body-parser';
const router = Router();

router.post('/searchPosts',requireAuthentication, bodyparser.json(), (req, res) => {

    let search = '%'+req.body.search+'%';
    let increment = Math.abs(Number(req.body.increment));
    let start =Math.abs( Number(req.body.ref));
    // drop request if not valid input
    if (isNaN(start) || isNaN(increment)){return res.status(400).json({"msg":"send a valid request!!!!"})}


    db.countPosts((postCount)=>{
        let q = "SELECT \
                    posts.content,\
                    posts.pid,\
                    posts.topic,\
                    posts.title,\
                    (SELECT users.username FROM users WHERE users.uid = posts.user) as username,\
                    posts.date,\
                    (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.post = posts.pid AND comments.modDeleted = 0) AS comment_count,\
                    (SELECT IFNULL(SUM(postVote.value),0) from postVote WHERE postVote.postID = posts.pid) AS vote_score\
                FROM\
                    posts\
                WHERE\
                    (UPPER(posts.title) LIKE UPPER(?) OR UPPER(posts.content) LIKE UPPER(?)) AND posts.modDeleted = 0\
                ORDER BY\
                    posts.date\
                DESC\
                LIMIT ?, ?";
        db.query(q, [search, search, start, ( increment)], (err,resPostsGroup)=>{
            if (err){console.error(err);}
            else{
                res.json({
                    total: postCount,
                    ref: start,
                    post: resPostsGroup
                });
            }
        });
        }
    )
});
    


export {router as searchPosts};
