import Router from 'express';
/* TODO: More relevant imports? */

const router = Router();

router.post('/logout', (req, res) => {
        // Log user out
        req.logout();
        res.status(200).json({msg: "Session destroyed."});
});

export { router as logout };