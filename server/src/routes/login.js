import Router from 'express';
import multer from 'multer';
import {passport} from "../middleware/index.js";
const router = Router();
const upload = multer();

router.post('/login',upload.none(), passport.authenticate('local'), async (req, res) => {
    if (req.isAuthenticated()){
        if(req.user.banned == 1){
            req.logout();
            res.status(403).json({"msg":"You are banned!"});
        }else{
            res.status(200).json({"msg":"OK, Logged in"});
        }
    }else{
        res.status(401).json({"msg":"Incorrect credentials"});
    }
});

export { router as login };