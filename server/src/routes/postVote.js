import Router from 'express';
import bodyparser from 'body-parser';
import {db, requireAuthentication} from "../middleware/index.js";

const router = Router();

router.post('/postVote',requireAuthentication,bodyparser.json(), (req, res) => {
    let postID = Math.abs( Number(req.body.pid));
    let userVote = Number(req.body.vote);
    // drop request if not valid input
    if (isNaN(postID) || isNaN(userVote) || userVote >1 || userVote < -1){return res.status(400).json({"msg":"send a valid request! "})}


    db.query("SELECT * FROM postVote where userID = ? AND postID = ?",[req.user.uid,postID],(err,result)=>{
        if (err) throw err;
        if(result.length >=1){
            db.query("UPDATE `postVote` SET `value`=? WHERE `voteID`= ? ", [userVote,result[0].voteID], (err,updateRes)=>{
                if (err) throw err;
                res.status(200).json({msg:"update vote OK"});
            });
        }else{
            db.query("INSERT INTO `postVote` (`voteID`, `userID`, `postID`, `value`) VALUES (NULL, ?, ?, ?);", [req.user.uid, postID, userVote], (err,insertRes)=>{
                if (err) throw err;
                res.status(200).json({msg:"Insert vote OK"});
            });
        }

    })

});

export { router as postVote };