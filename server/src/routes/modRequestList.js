import Router from 'express';
import { db, requireAuthentication } from '../middleware/index.js';

const router = Router();

router.use(Router.json());

router.post('/modRequestList', requireAuthentication, function(req, res) {
    let increment = Math.abs(Number(req.body.increment));
    let start = Math.abs(Number(req.body.ref));
    
    db.query("SELECT COUNT(uid) as user_count from users WHERE modRequest = 'sent'", (err, userCount) => {
        if (err) {
            throw(err);
        }
        let q = "SELECT \
                    users.uid,\
                    users.username,\
                    users.userCreated,\
                    (SELECT IFNULL(COUNT(posts.pid),0) FROM posts WHERE posts.user = users.uid) AS posts,\
                    (SELECT IFNULL(COUNT(comments.cid),0) FROM comments WHERE comments.user = users.uid) AS comments\
                FROM\
                    users\
                WHERE\
                    users.modRequest = 'sent'\
                GROUP BY\
                    1\
                ORDER BY\
                    users.uid\
                LIMIT ?, ?";
        
        db.query(q, [start, (increment)], (err, result) => {
            if (err) {
                throw (err);
            }
            res.status(200).json({
                total: userCount[0].user_count,
                ref: start,
                users: result
            })
        });
    });
});

export { router as modRequestList};