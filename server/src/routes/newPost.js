import Router from 'express';
import {db,requireAuthentication} from "../middleware/index.js";
import multer from "multer";
/* TODO: More relevant imports? */
const upload = multer();

const router = Router();

router.post('/newPost',upload.none(),requireAuthentication, (req, res) => {

    let TopicID = Math.abs(Number(req.body.tid));
    let title = req.body.title;
    let content = req.body.content;
    // Title valid number
    if(isNaN(TopicID)){return res.status(400).json({"msg":"send a valid request!!!!"})}

    // End if not valid (too long etc.)
    if(
        title.length >= 200 ||
        title.length < 3 ||
        content.length < 3 ||
        content.length >= 200000
    ){
        return res.status(400).json({"msg":"bad request: data in wrong format"});
    }

    db.query("INSERT INTO `posts` (`pid`, `user`, `topic`, `title`, `content`, `date`, `modDeleted`) VALUES (NULL, ?, ?, ?, ?, current_timestamp(), '0');", [req.user.uid,TopicID,title,content], (err,result)=>{
        if(err) throw  err;
        res.json({
            tid: TopicID,
            pid: result.insertId
        });
    })
});

export { router as newPost };