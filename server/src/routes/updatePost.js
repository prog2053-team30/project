import Router from 'express';
import multer from 'multer';
import { db, requireAuthentication } from '../middleware/index.js';

const router = Router();
const upload = multer();

router.post('/updatePost',requireAuthentication, upload.none(), (req, res) => {
    let postID = Math.abs(Number(req.body.pid));
    let postTitle = req.body.newTitle
    let postContent = req.body.newContent;

    req.on('data', chunk => {
        if ( req.body.length > 1e6) {
            return req.connection.destroy();
        }
    });

    //check if values too long
    if(postTitle.length > 200 || postContent.length > 20000|| isNaN(postID)){return res.status(400).json({"msg":"send a valid request!!!!"})}

    //check if user owns post
    db.query("SELECT * FROM posts WHERE posts.pid = ?", [postID], (err,result)=>{
        if (err) throw err;
        if(result.length >0){
            if (result[0].user === req.user.uid){
                db.query("UPDATE posts SET posts.title = ? , posts.content = ? WHERE posts.pid = ?",[postTitle,postContent,postID],  (err,result2)=>{
                   if (err) throw err;
                    res.status(200).json({msg:"Updated post :"+ postID});
                });
            }else{
                res.status(403).json({msg:"You don't own post "+ postID});
            }
        }else {
            res.status(400).json({msg:"No post with id"+ postID});
        }
    });

});

export { router as updatePost };