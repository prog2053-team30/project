import Router from 'express';
import {db, requireAuthentication} from "../middleware/index.js";
import bodyparser from 'body-parser';

const router = Router();

router.post('/modBlockedContentList',requireAuthentication,  (req, res) =>{
    if ((req.user.userType !== "moderator" && req.user.userType !== "admin")||
        req.user.banned === 1
    ){
        return res.status(400).json({msg:"you can't access delete/removed/blocked content"});
    }

    db.query("SELECT posts.`pid`, posts.`user`as user_ID, posts.`topic`, posts.`title`, posts.`content`, posts.`date`, posts.`modDeleted` , (SELECT users.username FROM users where users.uid = posts.user) as username FROM posts WHERE posts.modDeleted = 1", (err, res1)=>{
        if (err)throw err;
        db.query("SELECT comments.`cid`, comments.`post`, comments.`user` as user_ID, comments.`comment`, comments.`date`, comments.`modDeleted`,(SELECT users.username FROM users where users.uid = comments.user) as username FROM comments WHERE comments.modDeleted = 1", (err, res2)=>{
            if (err)throw err;
            db.query("SELECT `uid`, `username`, `userType`, `picture`, `modRequest`, `userCreated`, `banned` FROM users WHERE users.banned = 1", (err, res3)=>{
                res.status(200).json({posts:res1, comments:res2,users:res3});
            });
        });
    });

});

export { router as modBlockedContentList};