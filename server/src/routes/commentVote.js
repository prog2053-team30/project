import Router from 'express';
import bodyparser from 'body-parser';
import {db, requireAuthentication} from "../middleware/index.js";

const router = Router();

/*
 * Route to vote on a comment
 *
 * Users can use this route to cast a vote on a comment either positive (+1) or negative (-1).
 * Route checks if user ahs already commented and edits their previous vote if they have.
 * Users can only cast votes as them selves and no votes are anonymous.
 *
 * Required parameters are:
 *  - a valid comment id (numeric)
 *  - a valid vote (numeric)
 */

router.post('/commentVote',requireAuthentication,bodyparser.json(), (req, res) => {
    // capture inputs from request
    let commentID = Math.abs( Number(req.body.cid));
    let userVote = Number(req.body.vote);
    // drop request if not valid input
    if (isNaN(commentID) || isNaN(userVote) || userVote >1 || userVote < -1){return res.status(400).json({"msg":"send a valid request! "})}

    // check if tere exists a vote on that comment from that user
    db.query("SELECT * FROM commentVote where userID = ? AND commentID = ?",[req.user.uid,commentID],(err,result)=>{
        if (err) throw err;
        if(result.length >=1){
            // if there exists one already, alter that one with sent value
            db.query("UPDATE `commentVote` SET `value`=? WHERE `voteID`= ? ", [userVote,result[0].voteID], (err,updateRes)=>{
                if (err) throw err;
                // respond that all went well
                res.status(200).json({msg:"Update vote OK",voteID:result[0].voteID, was:result[0].value, shoudBe:userVote});
            });
        }else{
            // if there was no vote cast on that comment by this user already, then cast a new one!
            db.query("INSERT INTO `commentVote` (`voteID`, `userID`, `commentID`, `value`) VALUES (NULL, ?, ?, ?);", [req.user.uid, commentID, userVote], (err,insertRes)=>{
                if (err) throw err;
                // respond that all went well
                res.status(200).json({msg:"Insert vote OK"});
            });
        }

    })

});

export { router as commentVote };