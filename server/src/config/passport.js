import passport from 'passport';
import Strategy from 'passport-local';
import {db} from '../middleware/index.js';
import bcrypt from 'bcryptjs';

let strat = new Strategy({},
    function(username, password, cb) {
        db.getUserByUsername(username, function(err, user) {
            if (err) { return cb(err); }
            if (!user) { return cb(null, false); }
            bcrypt.compare(password,user.password).then(result => {
                if (result === false){
                    return cb(null, false);
                } else{
                    return cb(null, user);
                }
            });
        });
    }
);



export {strat as strat}