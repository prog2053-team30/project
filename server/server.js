"use strict";

import express from 'express';
import path from 'path';
import {passport} from './src/middleware/index.js';
import session from 'express-session';
import {
  register,
  login,
  logout,
  profile,
  topic,
  topics,
  newTopic,
  post,
  newPost,
  allPosts,
  searchPosts,
  deletePost,
  deleteComment,
  newComment,
  checkUserExists,
  modPageUserList,
  modRequestList,
  modRequestResponse,
  adminPageModList,
  profilePictureupload,
  checkTopicExists,
  postVote,
  commentVote,
  changeProfileInfo,
  changePassword,
  modRequest, updatePost, updateComment, modBlockedContentList, banUser
} from './src/routes/index.js'

const app = express();
const PORT = 8081;
app.use(session({ secret: 'keyboard cat', resave: false, saveUninitialized: false }))
app.use(passport.initialize());
app.use(passport.session());



// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.listen(PORT, () => {
  console.log('Running...');
})

app.use(express.static(path.resolve() + '/server'));


// App routes

// route all images
app.use("/uploads/",express.static( 'uploads'));

// Routing for login
app.use(login);

// Routing for registration
app.use(register);

//Routing for logging the user out
app.use(logout);

//Routing for requests about profile information
app.use(profile);

//Routing for requests about topic data.
app.use(topic);

//Routing for list of all topics
app.use(topics);

//Routing for posts related to a topic

//Routing for requests about the creation of new topics
app.use(newTopic);

//Routing for requests for post data
app.use(post);

//Routing for requests about the creation of new posts
app.use(newPost);

// routing for requests for data bout app posts
app.use(allPosts);

//routing for requests about search queries
app.use(searchPosts);

//Routing for requests about the deletion of posts
app.use(deletePost);

//Routing for requests about the deletion of comments
app.use(deleteComment);

//Routing for requests about the creation of new comment
app.use(newComment);

//Routing for requests about the existence of a user with a certain name
app.use(checkUserExists);

//Routing for requests about the existence of a topic with a certain title
app.use(checkTopicExists);

//Routing for fetching a user list for the modpage
app.use(modPageUserList);

// Routing for fetching users that have requested to become moderators
app.use(modRequestList);

// Routing for approving or denying mod requests
app.use(modRequestResponse);

//Routing for fetching a mod list for the adminpage
app.use(adminPageModList);

//routing for upload of profile pictures
app.use(profilePictureupload);

//routing for post voting
app.use(postVote);

//routing for comment voting
app.use(commentVote);

//routing for change profile info
app.use(changeProfileInfo);

// routing for changing password
app.use(changePassword);

// routing for request of usertype mod for users
app.use(modRequest);

// routing for post update (edits)
app.use(updatePost);

// routing for comment update (edits)
app.use(updateComment);

//routing for modPageContentBlockedLIST
app.use(modBlockedContentList);

// route for banning a user
app.use(banUser);